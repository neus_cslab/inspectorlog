# ![](IL-logo.png){width=60} Inspectorlog

Signature-based Intrusion detection system (SIDS) tool for detecting URI-based attacks using signatures on service trace files.

Accepts different trace formats and signatures in Snort, Nemesida and CRS formats.

This result has been partially financed by:

>    ![](mci.png){width=100}  **MCIN/ AEI/10.13039/501100011033/**
>
>  ![](feder-ja.png){width=100} **FEDER/ Junta de Andalucía** 
  

### Introduction
----
The most common SIDS (e.g. _Snort_, _Suricata_) operate on real-time traffic or captures in _pcap_ format. Currently, there is a lack of tools that apply the corresponding signatures on traces in text format. Therefore, to detect attacks from HTTP traces using this type of signatures, it has been necessary to develop a specific tool. In its first versions, basic functionality was implemented and only signatures in _Snort_ format were processed. In successive versions, its functionalities and capabilities have been complemented, and the tool can also use the public signatures used by _Nemesida_ and _ModSecurity_. _Inspectorlog_ only considers the URI and, where applicable, the method contained in each request for the detection of attacks, although it allows filtering of requests by the response code if available.

The tool has been evolving to enable different input formats for the traces and capacities that are increasingly closer to those of their corresponding versions on real traffic.
 
_Inspectorlog_ is in constant development, incorporating new elements and capabilities as it evolves. It has been validated with satisfactory results on several sets of traces, both public and private, and the few existing discrepancies can be explained based on the limitations described later.

### Functioning
----

The operation of _InspectorLog_ is based on the search for the signatures associated with each rule in the URIs based on string comparison, including the search for regular expressions. Its architecture and modules are detailed in [Arquitectura.md](./arquitectura.md).

The tools take as input a **trace file** (in different formats) and one or several **rule sets** (signatures). At the output, various lists can be generated containing the records that have activated any rule, along with details about the activated rules, as well as lists of records that do not activate any of the rules, that is, presumably legitimate. Therefore, it allows obtaining lists of clean requests and lists of attack requests.


In its current version (v3.6) it is implemented by two programs: _inspectorlog_ and _ms-inspectorlog_ that apply, respectively, the _Snort_ and _Nemesida_ type signatures on the one hand and the rules compatible with _ModSecurity_, on the other. For the latter, it uses the _libmodsecurity3_ library. Some additional tools are included for managing uri formats and segmentation.


### Usage
----

***inspectorlog***	

Analyzes trace files containing URIs in one of the defined formats and applies the signatures in Snort format contained in the files of the indicated directory and/or a rules file in Nemesida format. Generates a file with the URIs that trigger alerts and information about them. You can also generate a list of uris that do not trigger alerts.

```
inspectorlog -l logFile [-t <list|elist|apache|wellness|uri|telist>] [-b (labeled uris)] -a attack_output [-r ruleDir(snort)] [-m rulefile(nemeside)] [-o clean_log_output] [-f (raw_attacks)] [-n (nocase)] [-e (extended_alerts)] [-w (encoding warnings)] [-c (response code filtering)] [-x (strict response code filt)]

PARÁMETROS:
    -l logFile 	Log file to process 
    -t <list|apache|wellness|uri| elist| telist> 	(optional) Log file format. By default, it uses apache format.
    -b 			    (ptional) Identifiers (labels) for each request in the log file are in use.
    -a attack_output 	    Output file (attacks). U2URI format by default.
    -r ruleDir(snort)   	(optional) Directory with Snort rule files. ALL files in the directory are processed.
    -m rulefile(nemeside) 	(optional) Nemesida rules file.
    -o clean_log_output 	(optional) Output file with the trace elements that have not generated alerts (filtered, if applicable). Output format identical to input.
    -f          (optional) Generate the list of attacks without detection information. The output format will be identical to the input.
    -n          (optional) Apply the rules ignoring case in all cases. Otherwise, the rule will be used according to the existing “nocase” tag or the corresponding regular expression switch.
    -e          (optional) Enable extended alert information (includes alert message and sid). By default only the sid is indicated.
    -w          (optional) Generate warnings when percent encoded characters cannot be decoded.
    -c          (optional) Filter by response code for CR >=400.
    -x          (optional) Strict filtering by response code (for CR >=300).	
    
```
>
> :warning: All files found in the _ruleDir_ directory or in one of its subdirectories must belong to rules files.
>
> If you unzip the Snort rules files directly in said directory or in one of its directories, it is important to delete those files that do not correspond to Snort rules (such as "disclaimer" or license files). Otherwise the behavior may be unexpected.


***ms-inspectorlog***

Analyzes trace files containing URIs in one of the defined formats and applies rules compatible with _ModSecurity_ (e.g. CRS). Generates a file with the URIs that trigger alerts and information about them. You can also generate a list of uris that do not trigger alerts.

```
ms-inspectorlog -l logFile [-t<list|elist|apache|wellness|uri|telist> <list|elist|apache|wellness|uri|telist>] [-b (labeled uris)] -a attack_output -r modsecurity_conf_file [-o clean_log_output] [-e (extended_alerts)] [-w (encoding warnings)] [-c (response code filtering)]  [-x (strict response code filt)] [-d detailed_log_output]

PARÁMETROS:
    -l logFile             Log file to process
    -t <list|apache|wellness|uri|elist|telist> 	(opcional) Log file format. By default, it uses apache format.
    -b                  (optional) Identifiers (labels) for each request in the log file are in use.
    -a attack_output    Output file (attacks). U2URI format by default.
    -r modsecurity_conf_file    ModSecurity configuration file.
    -o clean_log_output         (optional) (optional) Output file with the trace elements that have not generated alerts (filtered, if applicable). Output format identical to input.
    -e      (opcional) Enable extended alert information (includes alert message and sid). By default only the sid is indicated.
    -w      Generate warnings when percent encoded characters cannot be decoded.
    -c      (optional) Filter by response code for CR >=400.
    -x      (optional) Strict filtering by response code (for CR >=300).	
    -d detailed_log_output  (optional) libModSecurity trace file.
```

## Input formats 

### Log files

The formats available for the trace file are:

- _list_: List of URIs without additional methods or fields
- _elist_: List of 'uri response_code response_size'
- _telist_: Idem _elist_ delimited by tabs
- _apache_: Apache standard format
- _wellness_: Specific format derived from Apache
- _uri_: Format used by the uri files of the tool ['ssm_v4'](/jedv/ssm) and later. The first line contains the number of uris. Each line includes uri's length and uri
   
Each line contains one record. All formats allow the optional use of a tag delimited by '[]' at the beginning of the line.

**Examples:**

**list**

/* URI */ 

    /sites/wdn4.mu.xs/files/calendario_sabados16-17_0_0.jpg
    /politecnica/node/45

**elist/telist** 

/* Method URI PROTOCOL" RESP_CODE RESP_SIZE */
    
    GET /sites/wdn4.mu.xs/files/calendario_sabados16-17_0_0.jpg	HTTP/1.1" 200 302880
    GET /politecnica/node/45 HTTP/1.1" 200 15978

**apache**

/* IP USERIDENTIFIER USERID [TIMESTAMP DIF] "METHOD URI PROTOCOL" RESP_CODE RESP_SIZE "-" "REFERER" */
    
    172.16.16.210 - - [02/May/2017:12:21:07 +0200]  "GET http://127.0.0.1/finger HTTP/1.1" 404 289 "-" "Wget/1.17.1 (linux-gnu)"  
    
**wellness**

/* TIMESTAMP NODE PLACE IP:PORT {server} "METHOD URI VER" CODE1 CODE2 */
    
    2017-06-22T06:25:15.356441+02:00 A-SQU-BAL haproxy[5518]: 10.128.2.64:46469 {www.pagina.es} "GET / HTTP/1.1" main_http_frontend WT_www_be/A-WTE

**uri**

/* Num_uris */<br>
/* SIZE URI */
    
    2
    55 /sites/wdn4.mu.xs/files/calendario_sabados16-17_0_0.jpg
    20 /politecnica/node/45

## Rules files

Three formats are considered:
- Standard _Snort_ rules (Talos/VRT/suricata/ETOpen)
- _nemesida_ rules (text file, obtained from https://rlinfo.nemesida-security.com/)
- Configuration and rules in standard ModSecurity format

### Snort

Rules are processed in the standard Snort format (Talos/VRT/suricata/ET).

Only the following fields are considered for the application of the rules: _http_method_, _http_uri_, _content_, _pcre_, _dsize_, _urilen_, _nocase_.

The _msg_, _reference_, _classtype_, and _sid_ fields are processed for referencing and indexing purposes.

The remaining fields are ignored.

### Nemesida

The _Nemesida_ rules file (Nemesida Community Edition) is a text file obtained from https://rlinfo.nemesida-security.com/ in which lines that do not contain rules and html formatting are removed. The result has the following format:

    RuleID Type Signature Tag Score Match_zone
    
    - RuleID: Rule identifier
    - Type: Can be RL, RLx, WL or WLx to indicate a blacklist rule (attack signature), blacklist using a regular expression, whitelist (legitimate request in all cases) or whitelist using a regular expression, respectively.
    - Signature: Signature string
    - Tag: Type/category of attack
    - Score: Score of the rule. Detection is activated if the total score for a URI exceeds the value 8
     
Example of rule 

    1 RL nwaftest Other 12 BODY|URL|ARGS|HEADERS

> :warning: 
> To enable compatibility with rules in Snort format, a **SID** is assigned to each Nemesida rule starting from the RuleID by adding 3000000.

### ModSecurity

The format used for _ModSecurity_ rules is standard and is managed from the corresponding configuration file.

## Output formats

### Attacks (fmto. U2URI)

The output regarding attacks is made to a file (option -a) and contains three sections:
- A first section with information about the loaded rules and their processing
- List of URIs classified as attacks (one record per line)
- Final statistics

The log format for an attack register contains the following fields:

    Packet [line_number][label] Uri [analyzed_URI] Nattacks [num_alerts]  [alert1_info] … [alertn_info]

The fields are tab delimited and the [tag] will only be present if it exists in the input trace file. The _'[alert1_info]'_ information about each alert depends on the _-e (extended)_ option, always including the SID of the activated rule and, optionally, the message associated with it.

Finally, a line is included with information on the number of records (processed, with alerts and number of alerts).

Lines that do not contain information related to a record begin with the '#' character to facilitate filtering.

**Example**
 

    # inspectorlog v3.6.0.1
    #—– Initializing Rules (SNORT) ———————
    # Rules directory : «/data/uri-data/biblio-ds/tmp/reglas»
    # Opening SNORT rule file ./tmp/reglas/http_uri-er-20220224-m2.rules… done
    # Rules: read [5918], erroneous [8], URI [5910]
    #—– Statistics (SNORT) ——————————
    # Read [9514] Snort rules, [9500] http-related, [13] with errors
    #—– Analysis results —————————-
    # Alerts & signatures generated from: /data/bin/inspectorlog -l raw-logs/apachelog-20170101.ltxt -t telist -b -r ./tmp/reglas/ -a rev3/attack-uris/apachelog-20170101-attacks.u2uri -n
    Packet [629][01-01-A000629] Uri [/politecnica/sites/all/modules/calendar/css/calendar_multiday.css?o8r51f] Nattacks [1] Signatures [882]
    Packet [763][01-01-A000763] Uri [/sites/wdn4.mu.xs/files/Biblioteca_Universitaria/calendario_web_sabados_2015-16.jpg] Nattacks [1] Signatures [882]
    Packet [805][01-01-A000805] Uri [/comunicacion/sites/all/modules/calendar/css/calendar_multiday.css?o86dyb] Nattacks [1] Signatures [882] 
    …
    # N. packets [35934], [44] with alerts, N. Alerts [49]

### Legitimate requests 

The output file (_clean_log_output_), if generated, has the same format as the input file, containing one log per line.

## Installation

### Prerequisites

- A Linux system with development tools in C is necessary to compile the program. It uses <code>_make_</code> to build the code.
- The <code>_pcre-devel_</code>, <code>_modsecurity3_</code> (https://github.com/SpiderLabs/ModSecurity) and <code>_uriparser_</code> (https://uriparser.github.io/) libraries are required to be installed. .

### Compilation

- To compile the program, edit the <code>_Makefile_</code> file and adjust the paths:
```
DESTDIR = ./
MODSECURITY = /usr/local/modsecurity
URIPARSER = /usr/local/lib
```

- Execute <code>make</code> in <code>src</code> directory. 

```
> make
> make install
```
- By default, it installs in the <code>_src_</code> directory.

## Auxiliary tools 

### C Programs

#### log-to-tablog

Auxiliary tool for processing trace files.
- Allows you to change the field delimiter from spaces to tabs (not all combinations are implemented).
- Allows you to filter records by response code (CR\<400)
- Allows you to remove labels on output

```
log-to-tablog -l logFile [-t <list|elist|apache|wellness|uri>] -o salida [-s <list|elist|apache|wellness|uri>][-c (response code filtering)] [-b (input labeled uris)] [-u (unlabeled output)] [-q (split path/query)] [-e (list erroneous)
```

#### uriparseri

Auxiliary tool to segment the uri fields and generate dictionaries with the values of the different segment types (_path/query/key/value_)

```
uriparseri  -l logFile -o outFile [-e] [-<u|v>] [-t <list|elist|apache|wellness|uri|telist>] [-c <clean log output>] [-b (labeled uris)][-q (output queries]) 
	-u: All the vocabulary in the same output file
	-v: Vocabulary per state
	-c <clean log output>: Clean uri file (filter out OOS URIS)
	-q: Output undecoded queries
	-e: Extended query analysis (use of ; and , as separators
```

### Python auxiliary tools (in <code>tools</code> directory)

> :warning: 
> Only basic error checks are implemented. Most tools assume properly formatted files are in use.

### log-label.py

Tool to generate requests' labels 

    log-label.py <COD|date> <num_digits> <input> <output> <-e (space as separator)>

### u2uri-to-labeledlist.py

Generate the list of labeled URIs from a U2URI formatted file.

    u2uri-to-labeledlist.py  <input (U2URI)> <output> 

### u2uri-attack-count.py

Counts the number of attack registers (attack URIS) and the total number of attacks

    u2uri-attack-count.py <input (U2URI)>


## Tests and examples

- Sample trace and rule files for testing are provided in the <code>tests</code> directory:

     - Rules
         - _nemesida-rules-bin.txt_: rules file in Nemesida format
         - _snort-rules_: directory with selected Talos distribution rules (Snort format) that affect URIs
         - _crs-rules_: directory with a very simple subset of OWASP CRS (v3.2.0) rules and configuration files
     - Log files in different formats. 
        - All the files contains the same 40 requests. First 20 should trigger alerts in <code>inspectorlog</code> and/or <code>ms-inspectorlog</code>.
        - Only <code>test-labeled.telist</code> contains labeled requests.


## Limitations

Given the nature of the input (log files), the tool presents some relevant limitations:

- Regarding the criteria related to flows (_flowbits_, _flow_). This limitation can lead to false positives (FP). It is insurmountable.

- In the current version, the rule fields that determine the relative positions between different rule components (e.g. _distance_) are ignored. It can lead to FP. This limitation could be alleviated in successive versions of the tool by using regular expressions to concatenate the expressions (_distance_).

- Problems have been identified in rules containing %00 (BUG). The problem is difficult to solve because the files are read line by line.

- Only the most common methods that include URIs are considered: _GET_, _POST_, _HEAD_, _PROPFIND_, _PUT_, _OPTIONS_.

## Disclaimer 

This software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.

This software is the result of research developments. The authors have made their best at trying to achieve a functional an usable software under laboratory conditions. Only limited error checks are made and it is assumed that input files conform the expected formats.

----

	INSPECTORLOG
	Version 3.6 - 11/02/2023

GPLv3 - 2013-2023 Jesús E. Díaz Verdejo - Laboratorio de ciberseguridad - Grupo de Investigación en Ingeniería Telemática – TIC 154 - https://dtstc.ugr.es/neus-cslab/
