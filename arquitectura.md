# InspectorLog: Architecture and Implementation Details

<code>![Readme](./README.md)</code>    
## Inspectorlog tool 

The way _InspectorLog works_ is based on searching for the signatures associated with each rule in the URIs based on string comparison, including the search for regular expressions. For this, a modular architecture is used (Fig. 1) in which the core is the detection module, whose function is to sequentially apply the set of selected rules to each of the requests in the traces.

> ![Fig. 1: Block diagram of InspectorLog](IL-architecture.png){width=450}
>
> Fig. 1: Block diagram of InspectorLog</center>


The tool takes as input a trace file and a set of rules (signatures) and generates at the output lists with the URIs considered legitimate (optional) and with the attack URIs. The processing carried out is:

- **Initialization**: During the initialization of the tool, the rules in the corresponding files are read one by one. The different fields of each rule are extracted (_rules parser_ module) and filtered to extract only those that affect the HTTP service (_rules filtering_ module). Depending on the type of format for the traces, methods not considered by the tool are also filtered. Finally, a database of signatures (_in-use signature_ block) is generated from the extraction of the patterns and conditions to be searched (_pattern extraction_ module).

- **Analysis**: Once the search rules and parameters have been initialized, the records in the trace file are read one by one (line by line), extracting the fields of interest from among those available according to the input format (_trace parser_ module). Depending on the active options, requests are filtered by response code (CR<300 or CR<400) (_CR filtering_ module). The URIs are then normalized (_URI normalization_ module), especially in relation to the so-called _percent encoding_. This normalization is carried out iteratively, in such a way that in the first iteration no modification is made and in each subsequent iteration the percent encoding sequences are decoded. The process is repeated as long as there are encoded characters (loop in Fig. 1) or a predefined limit is reached. Each of the iterations generates a request at the output that is processed by the detector (_detection_ module), which applies the signatures one by one in search of the patterns established in each one. In case of detection in any of the iterations, an attack log is generated that optionally includes information about the detection (activated signatures). Otherwise, the registration is considered legitimate and is added to the corresponding list.

URI normalization is necessary, even though RFC 3986 establishes a standard format for the representation of characters, because many systems do not take this recommendation into account. Additionally, some attacks use percent encoding representation as a cloaking method. What's more, we have confirmed that it is done iteratively (%25 is used to encode the '%' symbol and hide successive encodings, e.g. %252525...). This case is considered special as is handled in a single step by substituting the whole secuence. On the other hand, during the analysis of the Talos and ETOpen rules we have detected inconsistencies in relation to the use of percent encoding. To solve these problems we have established, as indicated above, an optional processing that, if the use of percent encoding is detected in a URI, applies all the rules, proceeds to decode the characters and reapplies all the rules, repeating the process as long as encoded characters exist.

A similar option is contemplated for the case of upper/lower case in the rules and in the character encoding. In this case, activating the corresponding option converts all characters to lowercase for processing.

### Customizable DEFINEs

The tool uses a set of DEFINES (see <code>inspector-common.h</code>) to stablish limits/thresholds for its operation. These DEFINEs are mainly related to the size of some elements or program behaviours. They can be changed as required. 

    // Fields/strings sizes
    #define MAX_URI_RULES 50000     // Maximun number of rules to store (STATIC ALLOCATION) 
    #define PATH_MAX 16385          // Maximum path size. Set according to RFC 3986)
    #define URILENGTH 16385         // Maximum length of a URI?? -> http://stackoverflow.com/questions/2659952/maximum-length-of-http-get-request
    #define WORDLENGTH 128          // Maximum size of a word - used for short char fields
    #define LINE_LENGTH	1024		// Maximum size of a line (for libmodsecurity fields	
    #define MAXLOG_LINE 600000		// Maximum length of a log line
    #define IPLENGTH 40				// Maximum length of a IP (40 to support IPv6)

    // Limits / operation
    
    #define MAX_ALERTS_PER_URI 124	// Maximum number of alerts triggered by a single URI
    #define MAX_DECODING_ITER 3		// Maximum number of URL decoding iterations
 
    // Snort rules
    
    #define SNORT_RULE_MAX 20096    // Maximum length of a Snort rule 
    #define RULES_DIR "rules"       // Default rules directory (Snort)
    #define MAX_REFERENCES 16		// Maximum number of references per rule
    #define MAX_PATTERNS 20			// Maximum number of content or alike fields (patterns to search for) per rule
    #define MAX_PCRE 6				// Maximum number of regular expressiones per rule
    #define CONTENT_LENGTH 1024		// Maximum length of a pattern
    #define MAX_BYTECODES 100		// Maximum lenght of a string of bytecodes
    
    // Filtering and error handling
    
    #define RESP_CODE_INVALID 400		// Response code from which records are dissmissed (if filtering by response code)
    #define RESP_CODE_STRICT 300		// Response code from which records are dissmissed (if filtering by strict response code)
    #define RULES_CRITICAL_LENGTH 1     // Whether long rules are considered critical error
    #define RULES_CRITICAL_FORMAT 1     // Whether errors in format in rules is considered critical
    #define RULES_CRITICAL_VALUES 1     // Whether errors in values of relevant fields in rules is considered critical
    #define RULES_CRITICAL_NVALUES 1    // Whether excesive content/pcre fields in rules is considered critical
    #define RULES_CRITICAL_MINIMAL 0    // Minimal mandatory fields considered critical
    #define RULES_CRITICAL_METHOD 0     // Whether not valid methods are considered critical
    
    // SIDS offsets - offset to add to SID number according to procedence
    
    #define SNORTSID_OFFSET 0			
    #define SURICATA_OFFSET 2000000
    #define NEMESIDA_OFFSET 300000000
    
    // Detection threshold 
    
    #define SCORE_THD 8				// Threshold for nemesida scores - Alert only if score is bigger - Set according to standard nemesida behaviour
    
    
### Valid methods

Only the following methods are considered valid for further analysis in the current version:

    GET
    POST
    HEAD
    PROPFIND
	PUT
	OPTIONS
    

### Snort rules parsing 

The rule fields considered for analysis, in the case of rules in standard Snort format (Talos/VRT/suricata/ET), are: _content_, _pcre_, _http_uri_, _http_method_, _dsize_, _urilen_ and _nocase_. For each rule, the _msg_, _reference_, _classtype_ and _sid_ fields are also stored to be displayed at the output, according to the established options, with each activation of the rule in the corresponding detection report. The remaining fields are ignored.

The details for each field are:

    Fields considered to apply a rule:
    - _content_           All _content_ field encountered are included up to MAX_PATTERNS fields 
    - _pcre_              All _pcre_ field encountered are included up to MAX_PCRE fields
    - _http_uri_          Considered and processed as a _content_ field.
    - _http_method_       Only 
    - _dsize_             Payload sizes and its comparisons (greater, lower, equal) are considered, although are only applied to URI field.
    - _urilen_            Same as _dsize_. In fact, they are handled as a single field.
    - _nocase_            Applied to the previous _content_ field.
    
    Informative fields:
    - msg               Rule's description 
    - reference         References in the rule (up to MAX_REFERENCES)
    - classtype         classtype field 
    - sid               SID 
    
### Nemesida rules parsing   
   
The file containing nemesida rules is processed as follows. 
- Lines starting by '#' are considered comments.
- All the fields in a rule (line) are processed. 
    - If any of the included affected sections (6th field) is URL, ARGS or BODY, the rule is stored and considered for attack detection. Otherwise, it is dismissed.
    - Accepted rules are classified as related to just the path, the query or the whole URL.
    
   
## ms-inspectorlog tool

The operation of the _ms-inspectorlog tool_ is based on the _libmodsecurity3_ library, which incorporates functions for loading the rules and applying the detection mechanisms. Therefore, the functional diagram is simplified. In this case, the initialization block corresponds to the call to the corresponding library function. In the same way, the detection module is implemented in said library and is executed on each request, according to the proper configuration files/rules. The tool postprocess the output of the detection module to extract attacks and the relevant information.

In this case, the URI normalization module is not applied, since the associated processing is incorporated into the library function.

Percent encoding is handled in the same way as for the _inspectorlog_ tool.


----

	INSPECTORLOG
	Version 3.6 - 11/02/2023

GPLv3 - 2013-2023 Jesús E. Díaz Verdejo - Laboratorio de ciberseguridad - Grupo de Investigación en Ingeniería Telemática – TIC 154 - https://dtstc.ugr.es/neus-cslab/
