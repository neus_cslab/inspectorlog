#!/usr/bin/python
# INSPECTORLOG AUX
# log-label.py
#
# Script to label each of the requests in a log file 
# Tag fmt: [COD-NUM]
# Compatible with [MONTH-DAY-TYPENUM]
#
# jedv - v1.2 20220806
# jedv - v1.1 20220109
# jedv - v1.0 20211007
#
# CHANGELOG
#   v1.1 - Use '[ ]' to delimit label and space as separator

from datetime import datetime
import sys
import os
import re
from operator import itemgetter

'''
MAIN FUNCTION

'''

# Variables

sep = False

# MAIN

if __name__ == '__main__':

    if ((len(sys.argv) == 5) or (len(sys.argv) == 6)):
    
        # Input
    
        if (len(sys.argv) == 6):
            if (sys.argv[5]=="-e"):
                sep = True;
            else:
                print("Invalid option {}".format(sys.argv[4]))
                print("FORMAT: log-label.py <COD|date> <num_digits> <input> <output> <-e (space as separator)>")
                sys.exit(-1)
                
        archivo = os.path.basename(sys.argv[2])
        try:
            g = open(sys.argv[3],'r')
        except IOError or FileNotFoundError:
            print("[{}] Error opening file {} ".format(sys.argv[3]))
            sys.exit(0)

        # Base code extraction 
        
        if (sys.argv[1] == "date"):
            mes =  " "
            dia = " "
            print("{}".format(archivo[0:11]))
            if (archivo[0:11] == "access_log-"):
                tipo = "A"
                mes = archivo[15:17]
                dia = archivo[17:19]
            elif (archivo[0:15] == "ssl_access_log-"):
                tipo = "S"
                mes = archivo[19:21]
                dia = archivo[21:23]
            else:
                print("[{}] Error searching for codes in file name {} ".format(archivo))
                sys.exit(0)
            base = mes + "-" + dia + "-" + tipo
        else:
            base = sys.argv[1]
            
        # Output

        salida = sys.argv[4] 
        try:
            f = open(salida,'w')
        except IOError or FileNotFoundError:
            print("[{}] Error opening file {} ".format(salida))
            sys.exit(0)
        
        ordinal = 0 
        ndigitos = int(sys.argv[2])
        if ((ndigitos < 3) or (ndigitos > 12)):
        	printf("Invalid numbering length {}".format(ndigitos))
        	sys.exit(0)
            
        for linea in g:
            ordinal += 1
            if (len(linea) >0):
#               f.write("[{}-{}-{}{:08d}] {}".format(mes,dia,tipo,ordinal,ndigitos,linea))
                if sep:
                    f.write("[{}{:0{}d}] {}".format(base,ordinal,ndigitos,linea))
                else:
                    f.write("[{}{:0{}d}]\t{}".format(base,ordinal,ndigitos,linea))

        g.close
        f.close

    else:
        print("FORMAT: log-label.py <COD|date> <num_digits> <input> <output> <-e (space as separator)>")
