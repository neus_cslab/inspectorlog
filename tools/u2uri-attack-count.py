#!/usr/bin/python
# INSPECTORLOG AUX 
# u2uri-attack-count.py
#
# Script to count total number of attack uris and total number of attacks from a U2URI file
#
# jedv - v2.2 20211112

from datetime import datetime
import sys
import os
import re
from operator import itemgetter

'''
MAIN FUNCTION

'''

# Variables

nuris = 0
nlineas = 0
nconataques = 0
nataques = 0
totaluris = 0

# MAIN

if __name__ == '__main__':

    if (len(sys.argv) == 2) :


        # Inicialization
        # Attacks file (U2URI)
        
        f = open(sys.argv[1],'r')

        nlineas = 0
        for linea in f:
            nlineas += 1
            if (len(linea) >0):
                if (linea[0:6] == "Packet"):        # Line with URI and attacks

                    nuris += 1
                    p = linea.find("Nattacks")
                    q = linea.find("]",p)

                    cab = linea[:p+10]
                    nat = int(linea[p+10:q])
                    if ((nat ) > 0):
                        nconataques += 1
                        nataques += nat 

        f.close
        print("# N. attack URIS [{}], N. Alerts [{}]\n".format(nconataques,nataques))

    else:
        print("FORMAT: u2uri-attack-count.py <input (U2URI)>")
 
