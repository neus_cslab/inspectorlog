#!/usr/bin/python
# SIVA 
# u2uri-to-labeledlist.py
#
# Script to extract uris lists with their labels from u2uri format (eliminates SIDS)
#
# jedv - v1.0 20220530

from datetime import datetime
import sys
import os
import re
from operator import itemgetter

'''
MAIN FUNCTION

'''

# Variables

sids = []
nlineas = 0
labels = 0


# MAIN

if __name__ == '__main__':

    if (len(sys.argv) == 3):

        # Inicialization
        # Attacks file (U2URI)
        
        f = open(sys.argv[1],'r')
            
        salida = sys.argv[2] 
        try:
            g = open(salida,'w')    
        except IOError or FileNotFoundError:
            print("[{}] Error opening file {} ".format(salida))
            sys.exit(0)
            
        nlineas = 0
        nuris = 0
        
        for linea in f:
            nlineas += 1
            if (len(linea) >0):
                if (linea[0:6] == "Packet"):        # Line with URI and attacks 
                
                    nuris += 1
                    p = linea.find("Nattacks")
                    if (p != -1):

                        cab = linea[7:p-1]
                        g.write("{}\n".format(cab))
                    
        g.close

        f.close
        print("# Processed [{}] uris \n".format(nuris))

    else:
        print("FORMAT: u2uri-to-labeledlist.py  <input> <output> ")
 