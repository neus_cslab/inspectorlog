/*
** INSPECTORLOG
** Todos los derechos reservados
** All rights reserved
**
** Copyright (C) 2017-2023, Jesús E. Díaz Verdejo
** Version 3.6 JEDV -- 11/02/2023
** Version 3.5.3 JEDV -- 10/02/2023
** Version 3.5.2 JEDV - - 21/09/2022
** Versión 3.5 JEDV - 27/03/2022
** Versión 3.4 JEDV - 25/11/2021
** Versión 3.0 JEDV - 19/12/2017
**
** Changes:
**
**   v3.6: Added method support for detection
**   v3.5.3: BUG solved: in non extended mode, all the nemesida alerts were printed despite not being counted in Nattacks if the URI is classified as attack
**   v3.5.2: Added strict response code filtering (CR<300)
**   v3.5.2: Added option to output attacks in raw mode (not U2URI)
**   v3.5.2: Lines filtered/parse errors output to stdout
**   v3.5.2: Added attack output file (U2URI)
**   v3.5: New input format TELIST
**   v3.4: Differentiated management of list and URI log types, including optional response code
**   v3.4: Optional labels for log entries
**   v3.4: Authomatic discard of not recognized methods (only Apache like logs)
**   v3.4: Considering NULL CHAR as potential part of a URI by explicitly using URI length - TODO: Not really used during comparison (engine)
**   v3.4: Added extended list format
*/

//INSPECTORLOG INCLUDES

#include <inspector-common.h>
#include <inspector.h>

#undef DEBUG

int nlineas = 0;

/****************************/
/* Public routines (inspectorlog)          */
/****************************/

/* Line by line analysis of a log file          */
/* Standard output is screen - Alerts are printed to screen */

void scan_logFile(const char *fileName){

	int total_alertas = 0;
	int npackets = 0;
	int npackets_with_alerts = 0;
	int n_nem_alerts = 0, n_alerts = 0;
    int n_filtered = 0;
    int n_errors = 0;
	struct log_map map;
	int tmp, n, s, parsed;
	int total_score = 0;
	bool wlr;

	//   char logLine[MAXLOG_LINE];
	char * logLine;
	char out_logline[MAXLOG_LINE];
	size_t lineLength;
	ssize_t read;

    //open file
    FILE * logFile = fopen(fileName, "r");
    FILE *fout;
    Apache_logEntry logEntry;
    int rules_detected[MAX_ALERTS_PER_URI], sid_detected[MAX_ALERTS_PER_URI], sid_sorted[MAX_ALERTS_PER_URI]; /* Lists of alerts triggered for a log line  */
	int scores[MAX_ALERTS_PER_URI];
	char *messages_detected[MAX_ALERTS_PER_URI]; /* Pointer to descriptions for sorted output */

    if(logFile != NULL){

        logLine = (char*) malloc (sizeof(char)*MAXLOG_LINE);
		lineLength = MAXLOG_LINE*sizeof(char);

        // Initialize log map

        init_log_map(&map);

        if (outputf) {
#ifdef DEBUG
            fprintf(stderr,"Opening clean output file <%s>\n",output_file);
#endif
            fout = fopen(output_file,"w");
            if (!fout) {
                fprintf(stderr,"[scan_logFile]: ERROR - Opening clean output file [%s]\n",output_file);
                exit(-1);
            }

#ifdef DEBUG
            fprintf(stderr," ... Done \n");
#endif
        };
        
        // Read first line (# of URIS) in an LOG_URI type
        if (log_type == LOG_URI) {
			read = getline(&logLine, &lineLength, logFile);

			// Write in the header the number of lines (maximum is the read value) - 1M lines top (output formatting)

			tmp = sscanf(logLine,"%d\n",&npackets);
			if (tmp != 1) {
                fprintf(stderr,"[scan_logFile]: ERROR reading the number of lines [%s]\n",output_file);
                free(logLine);
                fclose(faout);
                if (outputf) fclose(fout);
                exit(-1);
			}
			if (outputf) fprintf(fout,"%8d\n",npackets);
			npackets = 0;
			nlineas = 1;
    	} else nlineas = 0;

        // Main loop
        // Read and process each line

        for(int i=1; (read = getline(&logLine, &lineLength, logFile)) != -1; i++) {

            nlineas++;
 			if (read > MAXLOG_LINE) {
					fprintf(stdout,"[scan_LogFile]: WARNING - Line [%d] too long (%d chars) - DISMISSED\n", nlineas, read);
                    n_errors++;
					continue;
			}
			read++;		// To count for '\0'

            // Line parsing

            init_Apache_logEntry(&logEntry);

#ifdef DEBUG
			fprintf(stderr,"Parsing: [%s]\n",logLine);
#endif
			if (outputf) memcpy(out_logline,logLine,sizeof(char)*read);		// Copy original logline for output
            if ((log_type == LOG_APACHE) || (log_type == LOG_WELLNESS)) parsed = parse_apache_logEntry(logLine, &logEntry, map, read);
			else if (log_type == LOG_URI) parsed = parse_uri_logEntry(logLine, &logEntry, map, read) ;
			else if (log_type == LOG_LIST) parsed = parse_list_logEntry(logLine, &logEntry, map, read);
			else if (log_type == LOG_ELIST) parsed = parse_elist_logEntry(logLine, &logEntry, map, read);
			else if (log_type == LOG_TELIST) parsed = parse_telist_logEntry(logLine, &logEntry, map, read);
			if (parsed == -1) {
                fprintf(stdout,"[PARSE] [%d] %s\n",nlineas,out_logline);
				n_errors ++;
				continue;
			} else if (parsed == -2) { // Critical parse error
                fclose(faout);
                if (outputf) fclose(fout);
                free(logLine);
                fclose(logFile);
                exit(-1);
            };
#ifdef DEBUG
			fprintf(stderr,"Parsed URI [%s]\n",logEntry.URI);
#endif


            /* ---------- INDIVIDUAL RULES ----------
                Rules are invididually applied to each of the log records
				Only current record is considered
            */

            if (((resp_code) && (logEntry.status_code >= RESP_CODE_INVALID) ) || ((strict_resp_code) && (logEntry.status_code >= RESP_CODE_STRICT) ) ) {
#ifdef DEBUG
                if (outputf) fprintf(stdout,"[CODE_INV] [%d] %s",nlineas,out_logline);
#endif
                n_filtered ++;
                continue;
            };

            // Detect attack patterns in URI

			// Initialization
            int pos_matches = 0; 			//Number of positive matches via Individual Rules Scan
            for (int j=0; j < MAX_ALERTS_PER_URI; j++) { rules_detected[j]=0; sid_detected[j]=0; };

            npackets ++;
#ifdef DEBUGTIME
            time(&rawtime);
            fprintf(stderr,"Parsing packet [%d]= \"%s\"", i, ctime(&rawtime));
#endif
			// Detection
			// TODO: Compare all the string (even with \0 inside)

            pos_matches = detect_URI(logEntry.URI, rules_detected, logEntry.request_method);
            if (pos_matches > 0 ){ 		// Alerts triggered

                // Prepare to order alerts by SID

                for (n= 0; n< pos_matches;n++) {
					sid_detected[n] = URI_rules[rules_detected[n]]->sid;
					sid_sorted[n] = sid_detected[n];
				}

				// Handle nemesida scores - Check if total score is greater than threshold

				n_nem_alerts = 0;
				total_score = 0;
				wlr = false;
				for(n=0; n < pos_matches; n++){
					if (sid_detected[n] > NEMESIDA_OFFSET) {
						s = URI_rules[rules_detected[n]]->score;
						n_nem_alerts ++;
						if (s == 0) {
							wlr = true;
						} else {
							total_score += s;
						};
					};
				};
				if (wlr) total_score = 0;

                // Order alerts by SID

                qsort(sid_sorted, pos_matches, sizeof(unsigned int), compare);
				for (s=0; s < pos_matches; s++) {
					for (n = 0; n < pos_matches; n++) {
						if (sid_sorted[s] == sid_detected[n]) {
							messages_detected[s] = URI_rules[rules_detected[n]]->description;
							scores[s] = URI_rules[rules_detected[n]]->score;
						};
						continue;
					};
				};

				// Check if there are alerts to print

				if (total_score < SCORE_THD) n_alerts = pos_matches - n_nem_alerts;
				else n_alerts = pos_matches;
				
				if (n_alerts > 0) {  // There are remaining alerts after scoring

					// Alerts triggered: print output - u2uri compatible format

					total_alertas += n_alerts;

                    if (raw_attacks) {
                            fprintf(faout,"%s",out_logline);
                    } else  {
                        if (uri_labels) {
                            fprintf(faout,"Packet [%d]%s\tUri [%s]\tNattacks [%u]\tSignatures", i, logEntry.label,logEntry.URI, n_alerts);

                        } else {
                            fprintf(faout,"Packet [%d]\tUri [%s]\tNattacks [%u]\tSignatures", i, logEntry.URI, n_alerts);
                        };
                        
                        // Ouput attacks information

                        for(int n=0; n<pos_matches; n++){
                            s = sid_sorted[n];
                            if (ealert) {
                                if ((s < NEMESIDA_OFFSET) || (total_score >= SCORE_THD) ) {
                                    fprintf(faout,"\t[%s - SC %d - sid: %u]",messages_detected[n],scores[n],s);
                                };
                            } else if ((s < NEMESIDA_OFFSET) || (total_score >= SCORE_THD)) {
                                fprintf(faout,"\t[%u]",sid_sorted[n]);
                            };
                        }
                        fprintf(faout,"\n");
                    };
					npackets_with_alerts++;
				} else {	// Packet with void alerts - Print clean log if needed
					if (outputf) fprintf(fout,"%s",out_logline);
				};
            } else if (outputf) {
                fprintf(fout,"%s",out_logline);
            }

        }

        if (outputf) {
			if (log_type == LOG_URI) {	// If LOG_URI type, adjust the number of registers (1st line)
				rewind(fout);
				fprintf(fout,"%8d\n",npackets-npackets_with_alerts);
			};
			fclose(fout);
        };

		// Final summary for all the records - u2uri compatible

        if (log_type == LOG_URI) nlineas--;

        if (!raw_attacks) fprintf(faout,"# N. packets [%d], [%d] with alerts, N. Alerts [%d]\n",npackets, npackets_with_alerts, total_alertas);
        fprintf(stdout,"-> Processed [%d] lines, [%d] valid packets, [%d] with alerts, [%d] alerts total, [%d] filtered, [%d] malformed/erroneous\n",nlineas, npackets, npackets_with_alerts, total_alertas, n_filtered, n_errors);

        free(logLine);

        fclose(logFile);
    } else {
        fprintf(stderr,"[scan_logFile]: ERROR - Log path is incorrect = %s\n", fileName);
    }
}


