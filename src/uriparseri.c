/*
** INSPECTORLOG
** Todos los derechos reservados
** All rights reserved
**
** Copyright (C) 2017, Jesús E. Díaz Verdejo
** Version 3.6 JEDV - 07/07/2022
** Version 3.5 JEDV - 24/02/2022
** Versión 3.4 JEDV - 25/11/2021
** Versión 3.0 JEDV - 19/12/2017
** 

** uriparseri adaptado a partir del uriparser de la versión 5 de SSM 
** SSM
** (c) JEDV - 2010-2018 
*/

/* Changelog:
    v6.03: 28/12/2022 - Añadida opción para guardar los query antes de deshacer el percent encoding
    v6.02: 19/08/2022 - Corregido bug en descomposición del path cuando termina en /
    v6.01: 20/06/2022 - Modificación para que no se escriba nada en ficheros vocabulario si OOS en query
    v6: 07/07/2022 
        Entrada de archivos a partir de rutinas de inspectorlog

/* Version 5.1 - 2019 - Modificado para permitir query extendido */
/*      - Se permite campo ; al final del path              */
/*      - Se permite separación de valores por ,            */
/*      - Se permite separación de keys por ;               */
/* Versión 4.1.1 - 2018                                       */
/* Modificado para permitir valor nulo (como original)      */

/************************************************************/
/* uriparser                                                */
/*                                                          */
/* Segmentación de peticiones HTTP                          */
/* Genera los vocabularios a la salida                      */
/* Corregida cuenta de uris correctos a la salida           */
/************************************************************/

#include <inspector-common.h>
#include<math.h>
#include<locale.h>
#include <uriparser/Uri.h>
#include <getopt.h>

#undef DEBUG
#undef DEPURA

// Definitions

/* MÃ¡ximo nÃºmero de segmentos en un URI */
#define SEQEST 128

/* Estructura del uri */
typedef struct uris {
     int luri;              /* Longitud del URI*/
     char *uri;             /* Cadena del URI */
     int  estado[SEQEST];   /* Estados por los que pasa dentro del AEF */
     int  indice[SEQEST];   /* Indice de la palabra en vocabulario */
     int  seg_ini[SEQEST];  /* Posiciones del primer carácter de cada segmento del uri */
     int  seg_last[SEQEST]; /* Posiciones del primer carácter de cada segmento del uri */
     int  nseg;             /* numero de tokens para host, path y query */
     int  loc;              /* TamaÃ±o de memoria localizada */
  } ssm_uri_t;


// Globals

bool outputf = false;                // Output file 
int log_type = LOG_LIST;             // Log file format code (0 = apache standard, 1 = list, 2 = wellness, 3 = URI)
bool ealert = false;                 // Output alerts in extended format (msg/description + sid) -> Extended analysis in this program
bool uri_labels = false;            // Use of labels for log entries
int nlineas;                        // Number of lines parsed from a log file
bool unique_vocab = true;           // Single file for the vocabulary
bool clean_outputf = false;          // Output clean URIs to file
bool debug = false;
bool output_query = false;			// Output undecoded queries

// Not used globals (compatibility with inspector-common.h) 

bool nocase;                         // Activate global nocase (overrides per rule nocase) -> NOT USED
bool warns;                          // Generates warnings for not found %encodings -> NOT USED
bool resp_code;                     // Activate filtering by response code  -> NOT USED
time_t rawtime;
struct tm *timeinfo;

// Files/Input/Output

unsigned char log_path[PATH_MAX+1];         // Path to log file
unsigned char output_file[PATH_MAX+1];      // Path to output vocab file (basename if unique_vocab = true)
unsigned char clean_output_file[PATH_MAX+1];        // Path to output clean file

unsigned char attack_file[PATH_MAX+1];
FILE *faout;

FILE *logFile, *oh, *op, *ok, *ov, *oos, *co, *oq;


/* Arguments */

bool parse_args(int argc, char **argv){

    bool isOK = true;

    int c;
//    static int verbose_flag;

    if ((argc < 4) || (argc > 12))
        show_urihelp();


    while (1){

        static struct option long_options[] =
             {
               /* These options set a flag. */
//               {"verbose", no_argument,       &verbose_flag, 1},
//               {"brief",   no_argument,       &verbose_flag, 0},

               /* These options don't set a flag.
                  We distinguish them by their indices. */
               {"help",  no_argument,       0, 'h'},
               {"log_file",  required_argument, 0, 'l'},
               {"logtype", required_argument, 0, 't'},
               {"output", required_argument, 0, 'o'},
               {"clean_output", required_argument, 0, 'c'},
               {"extended_analysis", no_argument, 0, 'e'},
               {"labeled_uris", no_argument, 0, 'b'},
               {"unique_vocab", no_argument, 0, 'u'},
               {"perstate_vocab", no_argument, 0, 'v'},
               {"query_output", no_argument, 0, 'q'},
               {"debug",no_argument,0,'d'},
               {0, 0, 0, 0}
             };
           /* getopt_long stores the option index here. */
           int option_index = 0;


        c = getopt_long(argc, argv, "hl:t:o:c:ebuvqd", long_options, &option_index);
 
        /* Detect the end of the options. */
        if (c == -1)
            break;

#ifdef DEBUG
            printf("Option detected %c, %s\n",c,optarg);
#endif        
        switch (c){
            case 'l':
                strncpy((char*)&log_path, optarg, PATH_MAX);
                break;
            case 't':
                if (!strcmp(optarg,"list")) {
                    log_type = LOG_LIST;
                } else if (!strcmp(optarg,"apache")) {
                    log_type = LOG_APACHE;
                } else if (!strcmp(optarg,"wellness")) {
                    log_type = LOG_WELLNESS;
                } else if (!strcmp(optarg,"uri")) {
                    log_type = LOG_URI;
                } else if (!strcmp(optarg,"elist")) {
                    log_type = LOG_ELIST;
                } else if (!strcmp(optarg,"telist")) {
                    log_type = LOG_TELIST;
                } else {
                    printf("InspectorLog: log format [%s] not recognized\n",optarg);
                    exit(-1);
                }
                break;
            case 'o':
                strncpy((char *)&output_file, optarg, PATH_MAX);
                outputf = true;
                break;
            case 'c':
                strncpy((char *)&clean_output_file, optarg, PATH_MAX);
                clean_outputf = true;
                break;
            case 'e':
               ealert = true;
               break;
            case 'u':
               unique_vocab = true;
               break;
            case 'b':
                uri_labels = true;
                break;
            case 'h':
                show_urihelp();
                break;
            case 'v':
                unique_vocab = false;
                break;
            case 'q':
                output_query = true;
                break;
            case 'd':
                debug = true;
                break;
            default:
               show_urihelp();

        }
    }
#ifdef DEBUG
    printf(">> Command line arguments processed ...\n");
#endif

    /* Print any remaining command line arguments (not options). */
    if (optind < argc){
           printf ("[parse_clArgs] Argument(s) erroneous: ");
           while (optind < argc)
             printf ("%s ", argv[optind++]);
           putchar ('\n');
    }

    return isOK;
}

void show_urihelp(){
    fprintf(stdout,"FORMAT: uriparserii  -l logFile -o outFile [-e] [-<u|v>] [-t <list|elist|apache|wellness|uri|telist>] [-c <clean log output>] [-b (labeled uris)][-q (output queries]) \n");
    fprintf(stdout,"\t-u: All the vocabulary in the same output file\n");
    fprintf(stdout,"\t-v: Vocabulary per state\n");
    fprintf(stdout,"\t-c <clean log output>: Clean uri file (filter out OOS URIS)\n");
    fprintf(stdout,"\t-q: Output undecoded queries\n");
    fprintf(stdout,"\t-e: Extended query analysis (use of ; and , as separators\n");
    exit(EXIT_SUCCESS);
}


void close_files_and_exit(char *logLine) {

    if (logFile) fclose(logFile);
    if (oh) fclose(oh);
    if (op) fclose(op);
    if (ok) fclose(ok);
    if (ov) fclose(ov);
    if (oos) fclose(oos);
    if (co) fclose(co);
	if (oq) fclose(oq);
    
    if (logLine) free(logLine);
    exit(-1);
}

/****************/
/* MAIN PROGRAM */
/****************/


int main(int argc, char **argv){
    int i, n, parsed;
    bool haypath = false, oos_uri = false;
    struct log_map map;
    int nuris, nclean, tmp, read;
    ssm_uri_t u;
    UriParserStateA estado_uri;
    UriUriA uri;
    UriPathSegmentA *path;
    UriQueryListA *query, *q;
    int nquery;
    char archivo[PATH_MAX];
    char out_logline[MAXLOG_LINE];
    char host[URILENGTH], cpath[URILENGTH], ckey[URILENGTH], cvalue[URILENGTH], tmpval[URILENGTH];
    char *p, *k, *v;
    UriNormalizationMask Mask;
    char tmpquery[URILENGTH];
    //Parse command line arguments
    if( !parse_args(argc, argv))
        exit(EXIT_FAILURE);
    
    // Initialization (LIBURIPARSER)

// URI_NORMALIZE_SCHEME     Normalize scheme (fix uppercase letters)
// URI_NORMALIZE_USER_INFO  Normalize user info (fix uppercase percent-encodings)
// URI_NORMALIZE_HOST       Normalize host (fix uppercase letters)
// URI_NORMALIZE_PATH       Normalize path (fix uppercase percent-encodings and redundant dot segments)
// URI_NORMALIZE_QUERY      Normalize query (fix uppercase percent-encodings)
// URI_NORMALIZE_FRAGMENT   Normalize fragment (fix uppercase percent-encodings) 
    
//    Mask = {URI_NORMALIZED = 0, URI_NORMALIZE_SCHEME = 0 << 0, URI_NORMALIZE_USER_INFO = 0 << 1, URI_NORMALIZE_HOST = 0 << 2,   URI_NORMALIZE_PATH = 1 << 3, URI_NORMALIZE_QUERY = 1 << 4, URI_NORMALIZE_FRAGMENT = 1 << 5 } }
//      Mask = 1 << 3 & 1 << 4 & 1 << 5;
    Mask = URI_NORMALIZE_PATH | URI_NORMALIZE_QUERY;
    
    /* Files handling and initialization */

    char * logLine;
    logLine = (char*) malloc (sizeof(char)*MAXLOG_LINE);
    int lineLength = MAXLOG_LINE*sizeof(char);
    Apache_logEntry logEntry;
            
#ifdef DEBUG
    printf("Inicializando ...\n");
#endif
    
    if (!log_path) {    // Archivo de entrada
        fprintf(stdout,"[uriparser]: Log file not set\n");
        show_urihelp();
    } else {
        logFile=fopen(log_path,"r");
        if (logFile == NULL) {
            fprintf(stdout,"[uriparser]: ERROR opening log file [%s]\n",log_path);
            close_files_and_exit(logLine);;
        } else {        // Initialize log reading 
            // Initialize log map
            init_log_map(&map);
        }
    }
#ifdef DEBUG
    printf("Abierto archivo uris ...\n");
#endif
    if (strlen(output_file) < 1) {      // Output (vocabulary file(s))
        fprintf(stdout,"[uriparser]: Output file not set\n");
        show_urihelp();
    } else {        // Check whether a single or multiple output files
        if (unique_vocab) {
            oh= fopen(output_file,"w");
            if (!oh) {
                fprintf("[uriparser]: ERROR opening vocabulary ouput file - ALL [%s]\n",output_file);
                close_files_and_exit(logLine);
            };
            op = oh; 
            ok = oh;
            ov = oh;
            oos = oh;
            oq = oh;
        } else {
            strcpy(archivo,output_file);
            strcat(archivo,".host");
            oh= fopen(archivo,"w");
            if (!oh) {
                fprintf("[uriparser]: ERROR opening vocabulary ouput file - HOST [%s]\n",archivo);
                close_files_and_exit(logLine);
            };

            strcpy(archivo,output_file);
            strcat(archivo,".path");
            op= fopen(archivo,"w");
            if (!op) {
                fprintf("[uriparser]: ERROR opening vocabulary ouput file - PATH [%s]\n",archivo);
                close_files_and_exit(logLine);
            };
            
            strcpy(archivo,output_file);
            strcat(archivo,".key");
            ok= fopen(archivo,"w");
            if (!ok) {
                fprintf("[uriparser]: ERROR opening vocabulary ouput file - KEY [%s]\n",archivo);
                close_files_and_exit(logLine);
            };
            strcpy(archivo,output_file);
            strcat(archivo,".val");
            ov= fopen(archivo,"w");
            if (!ov) {
                fprintf("[uriparser]: ERROR opening vocabulary ouput file - VALUE [%s]\n",archivo);
                close_files_and_exit(logLine);
            };
            strcpy(archivo,output_file);
            strcat(archivo,".oos");
            oos= fopen(archivo,"w");
            if (!oos) {
                fprintf("[uriparser]: ERROR opening vocabulary ouput file - OOS [%s]\n",archivo);
                close_files_and_exit(logLine);
            };
            if (output_query){ 
                strcpy(archivo,output_file);
                strcat(archivo,".query");
                oq= fopen(archivo,"w");
                if (!oq) {
                    fprintf("[uriparser]: ERROR opening query ouput file [%s]\n",archivo);
                    close_files_and_exit(logLine);
                };
            }; 
        }
    };
#ifdef DEBUG
    printf("Abiertos archivos vocabulario y salida ...\n");
#endif  
    if (clean_outputf) {        // Archivo de salida con URIs conformes
        co= fopen(clean_output_file,"w");
        if (!co) {
            fprintf("[uriparser]: ERROR opening output compliant URI file [%s]\n",clean_output_file);
            close_files_and_exit(logLine);
        };
    };
        
    u.loc = 0;
    u.uri = NULL;
    
    // Start reading from logFile
#ifdef DEBUG
    printf("Comienza lectura archivo uris \n");
#endif  
    // Read first line (# of URIS) in an LOG_URI type
    if (log_type == LOG_URI) {
        read = getline(&logLine, &lineLength, logFile);

        // Write in the header the number of lines (maximum is the read value) - 1M lines top (output formatting)

        tmp = sscanf(logLine,"%d\n",&nuris);
        if (tmp != 1) {
            printf("[uriparseri]: ERROR reading the number of lines [%s]\n",log_path);
            close_files_and_exit(logLine);
        }
        if (clean_outputf) fprintf(co,"%8d\n",nuris);
        nuris = 0;
        nlineas = 1;
    } else nlineas = 0;

    /* Line by line processing */
    
    n=0;nclean=0;
    for(i=1; (read = getline(&logLine, &lineLength, logFile)) != -1; i++) {
        nlineas++;
        if (read > MAXLOG_LINE) {
            printf("[uriparseri]: WARNING - Line [%d] too long (%d chars) - DISMISSED\n", nlineas+1, read);
            continue;
        }
        read++;     // To count for '\0'
 
        if (read > APACHE_LOG_ITEMS * URILENGTH) {
            printf("[uriparseri]: WARNING - Line [%d] too long (%d chars) - DISMISSED\n", nlineas+1, read);
            continue;
        }
        
        // Line parsing

        init_Apache_logEntry(&logEntry);

#ifdef DEBUG
        printf("Parsing: [%s]\n",logLine);
#endif
        /* if (clean_outputf) */ 
        memcpy(out_logline,logLine,sizeof(char)*read);      // Copy original logline for output and logging
        if ((log_type == LOG_APACHE) || (log_type == LOG_WELLNESS)) parsed = parse_apache_logEntry(logLine, &logEntry, map, read);
        else if (log_type == LOG_URI) parsed = parse_uri_logEntry(logLine, &logEntry, map, read) ;
        else if (log_type == LOG_LIST) parsed = parse_list_logEntry(logLine, &logEntry, map, read);
        else if (log_type == LOG_ELIST) parsed = parse_elist_logEntry(logLine, &logEntry, map, read);
        else if (log_type == LOG_TELIST) parsed = parse_telist_logEntry(logLine, &logEntry, map, read);
        if (parsed == -1) {
#ifdef DEBUG
            printf("WARNING: Line [%d] parsed incorrectly [%s]\n",nlineas,out_logline);
#endif
            continue;
        };
        u.uri = logEntry.URI;
        nuris ++;
        oos_uri = false;

#ifdef DEBUG
        printf(" Parsed URI [%d] Linea [%d] [%s]\n",nuris,nlineas,logEntry.URI);
#endif
       
        // URI read -> start processing
       
        estado_uri.uri = (UriUriA *) &uri;
    
        if (uriParseUriA(&estado_uri, logEntry.URI) != URI_SUCCESS) {
            /* Hay un error en la segmentación */
//          printf("Error [%d] en posición [%s]\n",estado_uri.errorCode,estado_uri.errorPos);
            if (unique_vocab) fprintf(oos,"URI %d OOS [%s]\n",n+1,out_logline);             
            else fprintf(oos,"%s",out_logline);
            fprintf(stdout,"URI %d OOS [%s]\n",n+1,out_logline);
            oos_uri = true;
        } else {
            /* Procesamos el resultado */
            /* NOTA: No se consideran los campos no inluidos en el modelo */
            
            uriNormalizeSyntaxExA(estado_uri.uri,Mask);
            
#ifdef DEBUG   
            printf(">>>>>Processing uri [%d]: %s\n",nuris,logEntry.URI);
#endif
            /* Host */
            
            if (uri.hostText.first) {
                i=uri.hostText.afterLast-uri.hostText.first;
                if (i >= URILENGTH) {
                    fprintf(stdout,"[uriparser_test] ERROR in URI [%d] -> [%s]\n",n,out_logline);
                    fprintf(stdout,"\tHost name length greater than allowed\n");
                    close_files_and_exit(logLine);
                };
                strncpy(host,(char *)uri.hostText.first,i);
                host[i]='\0';
/*              if (unique_vocab) fprintf(oh,"[%d] HOST %s\n",n+1,host);
                else fprintf(oh,"%s\n",host); */
            } else {
                strcpy(host,"%7E");
            };
        
            /* Path */
            /* Se cuentan TODOS los paths vacios, tanto los de "/" como petición como los de final de cadena de path */
            
            path = (UriPathSegmentA *) uri.pathHead;
            cpath[0] = '\0';
            ckey[0] = '\0';
            cvalue[0] = '\0';
            if (!path) {    /* No hay path -> codificamos con espacio en el diccionario */
                strcpy(cpath,"%7E\n");
                haypath = true;
/*               if (unique_vocab) fprintf(oh,"%s ",cpath);
                 else fprintf(op,"%s\n",cpath); */
            } else {            
                while(path) {
                    i = path->text.afterLast - path->text.first;
                    if (i >= URILENGTH) {
                        // ERROR("uri_parser_test","ERROR: Longitud path excesiva","",-1);
                        printf("[uri_parser_test]: ERROR: Excesive path length [uri %d][len:%d]\n",n,i);
                        haypath = false;
                        path = path->next;
                    } else { 
                        haypath= true;
/*                        strncpy(cpath,path->text.first,i); 
                        cpath[i]='\0'; */
                        if (i > 0) {
                            strncat(cpath,path->text.first,i);
                            strcat(cpath,"\n");
                            
                            /* Modificación para contemplar la posibilidad de ; en último segmento de path (que se omitiría) */
                            
                            if ((!path->next) && false) {       /* Anulo la búsqueda de ; en path */
    /*                            if ( p=strrchr(cpath, ';')) *p = '\0'; */
                                  if (p = strchr(path->text.first,';')) { *p = '\n'; *(p+1) = '\0'; };
                            }
    /*                        if (unique_vocab) fprintf(oh,"%s ",cpath);
                            else fprintf(op,"%s\n",cpath); */
                            
                        };
                        path = path->next;
                    };
                };
//              if (strlen(cpath) == 0) strcpy(cpath,"%7E\n"); /* REDUNDANTE: Path nulo tratado como %7E */
            };

            /* Query */

            if ((uri.query.first != NULL) && (haypath)) {
#ifdef DEPURA
                printf("QUERY ANTES: %s\n",(char *)uri.query.first);
#endif

                if (output_query){
                    strncpy(tmpquery,uri.query.first, uri.query.afterLast - uri.query.first);
                    tmpquery[uri.query.afterLast - uri.query.first] = '\0';
                    fprintf(oq,"%s\n",tmpquery);
                }; 
                if (uriDissectQueryMallocA(&query, &nquery, (char *)uri.query.first,
                            (char *)uri.query.afterLast) != URI_SUCCESS) {
                    /* Failure */
//                    fprintf(ok, "QUERY %d OOS [%s]\n",n+1,out_logline);
                    oos_uri = true;
                    fprintf(stdout,"ERROR QUERY in uri [%d] OOS -> [%s]\n",n+1,out_logline);
                } else {
                    
#ifdef DEPURA
                    printf("QUERY analizado, nquery [%d]\n",nquery);
#endif
                    q = query;
                    
                    while(q) {
                        if (!q->value) {    /* No hay valor -> Anulamos el '=' de key si existe */
                                i = strlen(q->key);
                                p = (char *)q->key;
                                if (p[i-1] == '=') p[i-1]='\0';
                        };
                        
                        /* Posibilidad de que haya dos o más keys (sin value) */
                        
                        if (ealert) {
                            
                            v = strtok((char *)q->key,";");
                            if (!v) {
/*                                fprintf(ok, "QUERY %d EXT ERROR/OOS [%s]\n",n+1,out_logline); */
                                oos_uri = true;
                                fprintf(stdout,"ERROR EXT KEY in uri [%d]  -> [%s]: key [%s]\n",n+1,out_logline,q->key);   
                            }
                            while ( v != NULL) {
/*                                if (unique_vocab) fprintf(ok,"KEY %s ",v);
                                else fprintf(ok,"%s\n",v); */
                                strcat(ckey,v);
                                strcat(ckey,"\n");
                                v = strtok(NULL,";");
                            };
 
                        } else  {
                            strcat(ckey,q->key);
                            strcat(ckey,"\n");
                            
/*                            if (unique_vocab) fprintf(ok,"KEY %s ",q->key);
                            else fprintf(ok,"%s\n",q->key); */
                        } 
                        
                        /* Lo que queda es value, aunque es posible que haya parejas */
                        
                        if (q->value) { 

#ifdef EMPTYVAL
                             if (strlen(q->value) == 0) {
                                if (unique_vocab) fprintf(ov,"VALUE %%20 ");
                                else fprintf(ov,"%%20\n");
                            } else {
#else                       
                            if (strlen(q->value) > 0) {
#endif

                                if (ealert) {
                                    
                                    v = strtok_r((char *)q->value,";",&p);
                                    if (!v) {
                                        oos_uri = true;
/*                                        fprintf(ov, "VALUE %d EXT ERROR/OOS [%s]\n",n+1,out_logline); */
                                        fprintf(stdout,"ERROR EXT VALUE in uri [%d]  -> [%s]: value [%s]\n",n+1,out_logline,q->value);   
                                    }
                                    
                                    /* Primer value (parseamos buscando varios separados por , */
                                    
                                    strcpy(tmpval,v);
                                    k = strtok(tmpval,",");
                                    
                                    while (k) {
                                        strcat(cvalue,k);
                                        strcat(cvalue,"\n");
/*                                        if (unique_vocab) fprintf(ov,"VALUE %s ",k);
                                        else fprintf(ov,"%s\n",k); */
                                        
                                        k = strtok(NULL,",");
                                    };
                                    
                                    while (v = strtok_r(NULL,";",&p)) { /* Procesamos las restantes parejas */
                                        
                                        k = strchr(v,'=');
                                        if (k) {    /* Puede haber value */
                                        
                                            strncat(ckey,v,k-v);
                                            strcat(ckey,"\n");
/*                                            strncpy(ckey,v,k-v);
                                            if (unique_vocab) fprintf(ok,"KEY %s ",ckey);
                                            else fprintf(ok,"%s\n",ckey);                                           
*/                                            
                                            if (*(k+1) != '\0') {   /* Hay value */
                                                strcpy(tmpval,k+1);
                                                
                                                k = strtok(tmpval,",");
                                                while (k) {
                                                    strcat(cvalue,k);
                                                    strcat(cvalue,"\n");
                                                    /* if (unique_vocab) fprintf(ov,"VALOR %s ",k);
                                                    else fprintf(ov,"%s\n",k);   */                                                  
                                                 
                                                    k = strtok(NULL,",");
                                                }
                                                
                                            }
                                            
                                        } else {    /* No hay value */
                                        
                                            strncat(ckey,v,k-v);
                                            strcat(ckey,"\n");
                                        
                                            /* if (unique_vocab) fprintf(ok,"KEY %s ",v);
                                            else fprintf(ok,"%s\n",v);     */                                        
                                        }
                                    }
                                    
                                } else {    /* Caso no extendido */
                                    
                                    strcat(cvalue,q->value);
                                    strcat(cvalue,"\n");
                                    
                                    /* if (unique_vocab) fprintf(ov,"VALOR  %s ",q->value);
                                    else fprintf(ov,"%s\n",q->value); */
                                   
                                };
                            }
                        }
                        q = q->next;
                    };
                };
                uriFreeQueryListA(query);
            };
#ifdef DEBUG
            printf("Finalizado análisis: OOS [%d] Path [%s]\n",oos_uri, cpath);
#endif            
           
            /* Escribimos los vocabularios y salidas en archivos */
        
            if (oos_uri == false) {

                /* Salida limpia */
                
                if (clean_outputf) {
                    fprintf(co,"%s",out_logline);
                }
                nclean++;
                    
                /* Host */
                
                if ((debug) && (uri_labels)) {
                    if (unique_vocab) fprintf(oh,"%s\n",logEntry.label);
                    else {
                        fprintf(op,"%s\n",logEntry.label);
                        fprintf(ok,"%s\n",logEntry.label);
                        fprintf(ov,"%s\n",logEntry.label);
                    };
                }
                if (strcmp(host,"%7E")) {
                    if (unique_vocab) fprintf(oh,"[%d] HOST %s\n",n+1,host);
                    else fprintf(oh,"%s\n",host);
                } else {
                    if (unique_vocab) fprintf(oh,"[%d] HOST null \n",n+1);
                    else fprintf(oh,"%s\n",host);
                }
                
                /* Path */
                
                if (unique_vocab) fprintf(oh,"PATH ");
                if (unique_vocab) fprintf(oh,"%s",cpath);
                else fprintf(op,"%s",cpath);
                
                /* Key */
                
                if (strlen(ckey) > 0) { 
                    if (unique_vocab) fprintf(oh,"KEY ");
                    if (unique_vocab) fprintf(oh,"%s",ckey);
                    else fprintf(ok,"%s",ckey);
                };
                
                /* Valor */
                
                if (strlen(cvalue) > 0) {
                    if (unique_vocab) fprintf(oh,"VALUE ");
                    if (unique_vocab) fprintf(oh,"%s",cvalue);
                    else fprintf(ov,"%s",cvalue);           
                };
                
            } else {    /* OOS en QUERY */
                if (unique_vocab) fprintf(oos,"URI %d OOS [%s]\n",n+1,out_logline);             
                else fprintf(oos,"%s",out_logline);
            }
            
        };
/*      if (unique_vocab) fprintf(oh,"\n"); */
        n++;
        uriFreeUriMembersA(&uri);       
    };
    
    /* URI read loop finished, finishing ... */
    
    if (clean_outputf) {
        if (log_type == LOG_URI) {  // If LOG_URI type, adjust the number of registers (1st line)
            rewind(co);
            fprintf(co,"%8d\n",nclean);
        };
        fclose(co);
    };

    // Final summary for all the records - u2uri compatible

    printf("# N. uris [%d], [%d] OOS\n",nuris, nuris-nclean);

    if (logFile) fclose(logFile);
    if (oh) fclose(oh);
    if (op) fclose(op);
    if (ok) fclose(ok);
    if (ov) fclose(ov);
    if (oos) fclose(oos);
    if ((output_query) && (oq) ) fclose(oq);
    
//  if (logLine) free(logLine);
        
    return(0);
        
}
