/*
** INSPECTORLOG
** Todos los derechos reservados
** All rights reserved
**
** Copyright (C) 2017-2022, Jesús E. Díaz Verdejo
** Version 3.5.2 JEDV - 21/09/2022
** Versión 3.4 JEDV - 25/11/2021
** Versión 3.0 JEDV - 19/12/2017
** 
**
** No changes from v3.4
**
*/



#ifndef __ARGUMENTS
#define __ARGUMENTS

//C INCLUDES
#define WLENGTH 128

#include <stdbool.h>

bool parse_clArgs(int argc, char **argv);
bool parse_msArgs(int argc, char **argv);

void show_help();
void show_mshelp();

#endif

