/*
** INSPECTORLOG
** Todos los derechos reservados
** All rights reserved
**
** Copyright (C) 2017-2023, Jesús E. Díaz Verdejo
** Version 3.6 JEDV -- 11/02/2023
** Version 3.5.3 JEDV - - 07/02/2023
** Version 3.5.2 JEDV - - 21/09/2022
** Version 3.5.1 JEDV - 07/08/2022
** Version 3.5 JEDV - 24/02/2022
** Versión 3.4 JEDV - 25/11/2021
** Versión 3.0 JEDV - 19/12/2017
**
** Changes:
**
**   v3.6: Added function to assign method string
**   v3.5.3: Added function to check and set valid methods - New set method procedures
**   v3.5.2: No changes so far
**   v3.5.1: Bug in elist format when HTTPS or no protocol
**	 v3.5.1: Bug in urilength evaluation while splitting APACHE string
**	 v3.5.1: Changed: protocol not mandatory in APACHE format
**	 v3.5.1: Added support for HTTPS/X in apache format
**	 v3.5.1: Bug in apache format parsing for complex referer (number of space separated fields exceeds APACHE_LOG_TOKENS)
**	 v3.5.1: IPv6 considered as valid IP. No splitting for IP:port
**	 v3.5.1: Bug fixed: update map index when labels only if map >= 0
**	 v3.5.1: Bug fixed for URI len in uri type when http is present
**	 v3.5: Bug fixed for elist with ending spaces
**	 v3.5: Added HTTP/2 as a valid protocol 
**	 v3.5: New format: telist (tab separated elist)
**	 v3.5: Some bug fixes in apache-like log parsing
**   v3.5: Proper handling of apache-like logs when fields include spaces 
**   v3.5: Added PUT method as valid for processing
**   v3.4: Differentiated management of list and URI log types, including optional response code
**   v3.4: Optional labels for log entries
**   v3.4: Authomatic discard of not recognized methods (only Apache like logs)
**   v3.4: Considering NULL CHAR as potential part of a URI by explicitly using URI length - TODO: Not really used during comparison (engine)
**   v3.4: Added extended list format
*/

/* TODO:
	apache-like format is split using spaces as delimiter. Will fail if URI contains uncoded spaces
	
*/

//INSPECTORLOG INCLUDES

#include <inspector-common.h>

extern int nlineas;

/****************************/
/* Auxiliary routines       */
/****************************/

_requestMethod check_method(char *str) {
    
    
	if( strcasestr(str, "GET") != NULL ) {
		return(GET);
	} else if(strcasestr(str, "HEAD") != NULL ) {
		return(HEAD);
    } else if(strcasestr(str,"POST") != NULL) {
		return(POST);
	} else if (strcasestr(str,"PROPFIND") != NULL) {
		return(PROPFIND);
	} else if (strcasestr(str,"PUT") != NULL) {
		return(PUT);
    } else if (strcasestr(str,"OPTIONS") != NULL) {
        return(OPTIONS);
	} else {
#ifdef DEBUG
		printf("[check_method]: Invalid method [%s] found in line [%d]\n",str, nlineas);
#endif
		return(OTHER);
	}
}

void method_str(_requestMethod method, char *strmethod) {
	if( method == GET) strcpy(strmethod,"GET"); 
	else if ( method == HEAD) strcpy(strmethod,"HEAD");  
	else if ( method == POST) strcpy(strmethod,"POST");  
	else if ( method == PROPFIND) strcpy(strmethod,"PROPFIND");  
	else if ( method == PUT) strcpy(strmethod,"PUT");  
	else if ( method == OPTIONS) strcpy(strmethod,"OPTIONS");  
	else if ( method == OTHER) strcpy(strmethod,"OTHER");  
	else strcpy(strmethod,"");
	
}

/************************************************/
/*  IP handling functions                       */
/************************************************/

//Convert ip from string to 4 bytes representation
void convert_ipv4(const char ip_string[16], unsigned char ip_byte[4]){

    char * token = strtok((char *)ip_string, ".");
    for (int n=0; n<4; n++) {
        int tmp = atoi(token);
        ip_byte[n] = (unsigned char)tmp;
#ifdef DEBUG
        printf ("%s\n", token);
#endif
        token = strtok(NULL, ".");
    }
}

unsigned int dec_toIP(unsigned char ip_address[4]){

    unsigned int IP = 0;
    IP |= (ip_address[0] << 24 );
    IP |= (ip_address[1] << 16 );
    IP |= (ip_address[2] <<  8 );
    IP |= (ip_address[3]       );

    return IP;
}

// Initialization of mapping of fields from log file

void init_log_map(log_map *m ) {

   /* Field mapping (dependent on the log type) - OPTIONAL LABEL */
    /* TYPE WELLNESS (10 fields)
    2017-06-22T06:25:15.356441+02:00 A-SQU-BAL-HAP03 haproxy[5518]: 10.128.2.64:46469 {www.wtelecom.es} "GET / HTTP/1.1" main_http_frontend WT_www_be/A-WTE-INF-WEB03
    TIMESTAMP NODE PLACE IP:PORT {server} "METHOD URI VER" CODE1 CODE2

    TYPE APACHE (12 fields)
    172.16.16.210 - - [02/May/2017:12:21:07 +0200]  "GET http://127.0.0.1/finger HTTP/1.1" 404 289 "-" "Wget/1.17.1 (linux-gnu)"
    37.152.139.155 - - [07/Nov/2013:17:00:31 -0800] "GET /2003/padron.html HTTP/1.1" 200 11800 "-" "Java/1.7.0_15" "ajedreznd.com"
    IP USERIDENTIFIER USERID [TIMESTAMP DIF] "METHOD URI PROTOCOL" CODE1 CODE2 "-" "REFERER"

    TYPE LIST (1 field)
	(URI in the first field)

	TYPE URI (2 fields)
	(URI in the second field -first line contains the total number of registers

	TYPE ELIST (5 fields)
	GET /ingenieros/node HTTP/1.1" 200 26091
	
	OPTIONAL LABEL (FIRST FIELD []) IN ALL FORMATS

    */

    if (log_type == LOG_APACHE) {
        m->ip = 0;
        m->useridentifier = 1;
        m->userid = 2;
        m->timestamp = 3;
        m->dif = 4;
        m->method = 5;
        m->uri = 6;
        m->protocol = 7;
        m->status_code = 8;
        m->return_size = 9;
        m->referer = 10;
        m->user_agent=11;
		m->nfields = 12;
    } else if (log_type == LOG_WELLNESS) {
        m->ip=3;
        m->useridentifier = -1;
        m->userid = -1;
        m->timestamp = 0;
        m->dif = -1;
        m->method = 5;
        m->uri = 6;
        m->protocol = 7;
        m->status_code = -1;
        m->return_size = -1;
        m->referer = -1;
        m->user_agent = -1;
		m->nfields = 10;
    } else if (log_type == LOG_LIST) {
        m->ip=-1;
        m->useridentifier = -1;
        m->userid = -1;
        m->timestamp = -1;
        m->dif = -1;
        m->method = -1;
        m->uri = 0;
        m->protocol = -1;
        m->status_code = 1;
        m->return_size = -1;
        m->referer = -1;
        m->user_agent = -1;
		m->nfields = 1;
    } else if (log_type == LOG_URI) {
        m->ip=-1;
        m->useridentifier = -1;
        m->userid = -1;
        m->timestamp = -1;
        m->dif = -1;
        m->method = -1;
        m->uri = 1;
        m->protocol = -1;
        m->status_code = 2;
        m->return_size = -1;
        m->referer = -1;
        m->user_agent = -1;
		m->nfields = 2;
    } else if (log_type == LOG_ELIST) {
        m->ip=-1;
        m->useridentifier = -1;
        m->userid = -1;
        m->timestamp = -1;
        m->dif = -1;
        m->method = 1;
        m->uri = 2;
        m->protocol = 3;
        m->status_code = 4;
        m->return_size = 5;
        m->referer = -1;
        m->user_agent = -1;
		m->nfields = 5;
	}

	if (uri_labels) { // Renumber all the mappings to include labels
		m->label = 0;
		if (m->ip >= 0) m->ip ++;
        if (m->useridentifier >= 0) m->useridentifier ++;
        if (m->userid >= 0) m->userid ++;
        if (m->timestamp >= 0) m->timestamp ++;
        if (m->dif >= 0) m->dif ++;
        if (m->method >= 0 ) m->method ++;
        if (m->uri >= 0) m->uri ++;
        if (m->protocol >= 0) m->protocol ++;
        if (m->status_code >= 0) m->status_code ++;
        if (m->return_size >= 0) m->return_size ++;
        if (m->referer >= 0) m->referer ++;
        if (m->user_agent >= 0) m->user_agent++;
		if (m->nfields >= 0) m->nfields ++;
	} else m->label=-1;

    return;
}

/* Extraction of field values from a log line  */

int parse_apache_logEntry(char * logLine, Apache_logEntry * logEntry, log_map map, int logLinelen){

#define APACHE_LOG_TOKENS 24 //Maximum number of tokens on Apache entries

    //Temp strings in tokenizer process
    char * tmp_str[APACHE_LOG_TOKENS+1], tmp[WORDLENGTH];
    char * p;
    int pos,n, offset, nf, urilength = 0;
	int more = false, existprot = true;
	
	// Remove ending \n if exist
	
	while (logLine[logLinelen -2] == '\n') {
		logLinelen --;
		logLine[logLinelen -1] = '\0';
	};

    //Tokenizer by white-space
    tmp_str[0] = strtok((char *)logLine, " ");
	nf = (uri_labels) ? APACHE_LOG_TOKENS +1 : APACHE_LOG_TOKENS;
    for (n=1; n < nf && tmp_str[n-1]!=NULL; n++){
        tmp_str[n] = strtok(NULL, " ");
		if (n == (map.uri+1)) {
			urilength = tmp_str[n]-tmp_str[n-1] -1;		// Separator is assumed to be a single space
#ifdef DEBUG2
			printf("\tUrilength %d\n ",urilength);
#endif
		}
#ifdef DEBUG2
        printf("\t%d [%s]",n,tmp_str[n]);
#endif
    }
	n--;
#ifdef DEBUG2
        printf("\n[%d fields] Urilength [%d]",n, strlen(tmp_str[map.uri]) );
#endif
	
	if (uri_labels) {
		if (tmp_str[0][0] == '[') {
			strcpy(logEntry->label,&(tmp_str[0][0]));
			p = strchr(logEntry->label,']');
			if (p) *(p+1) = '\0';
			else {
				fprintf(stderr,"[parse_apache_logEntry]: ERROR - Bad formatted label [%s]`END line [%d] - Probably the type of log is incorrect \n",logEntry->label,nlineas);
				return(-2);
			};
		} else {
				printf("[parse_apache_logEntry]: ERROR - Bad formatted label [%s], line [%d] - Probably the type of log is incorrect \n",tmp_str[0],nlineas);
				return(-2);			
		};
	};

    /* Basic check of log type, n contains the number of fields plus 1 */

    if (((log_type == LOG_APACHE) && (n<10)) || ((log_type == LOG_WELLNESS) && (n < 11)) ) {
        if (nlineas == 1) {
            printf("[parse_apache_logEntry]: ERROR - Incorrect number of fields [%d], line [%d] - Probably the type of log is incorrect \n",n,nlineas);
            return(-2);
        } else  {
            printf("[parse_apache_logEntry]: Parse error - line [%d] [",nlineas);
            for (pos=0;pos<n-2;pos++) printf("%s ",tmp_str[pos]);
            printf("%s]\n",tmp_str[n-2]);
            return(-1);
        }
    }

    offset = 0;

    /* Field processing */

    if (map.ip >= 0) {
        if (log_type == LOG_WELLNESS) {         /* Separation IP:port - IPv4 assumed */

            p = strstr(tmp_str[map.ip],":");
            if (p) {
				strcpy(logEntry->port, p);
				*p = '\0';
			};

        };
        if (strlen(tmp_str[map.ip]) > IPLENGTH) {
            printf("[parse_logEntry]: IP field with erroneous format [%s], line [%d]\n",tmp_str[map.ip],nlineas);
            return(-1);
        }

        /* convert_ip(tmp_str[map.ip], logEntry->ip_address); */

		strcpy(logEntry->ip_address, tmp_str[map.ip]);

    }

    if (map.useridentifier >= 0)
        if (tmp_str[map.useridentifier])
            strcpy(logEntry->user_identifier,tmp_str[map.useridentifier]);

    if (map.userid >0)
        if (tmp_str[map.userid])
            strcpy(logEntry->user_id, tmp_str[map.userid]);
#ifdef DEBUG2
		printf("\tUserid  [%s]\n",logEntry->user_id);
#endif
    if (map.timestamp >= 0)
        if(tmp_str[map.timestamp]) {
            if (log_type == LOG_APACHE) {
                strcpy(tmp, tmp_str[map.timestamp]);
                strcat(tmp, tmp_str[map.dif]);
                strptime(tmp, "[%d/%b/%Y:%T %z]", &logEntry->time);
            } else if (map.timestamp >0) strptime(tmp_str[map.timestamp], "[%d/%b/%Y:%T %z]", &logEntry->time);
        }

    /* Valid methods: MANDATORY */

    if (!tmp_str[map.method]) {
        printf("[parse_apache_logEntry]: Method not found - Line [%d] \n",nlineas);
        return(-1);
    }

/* Old version - Keep till validation of new version 
	if( strstr(tmp_str[map.method], "GET") != NULL ) {
		logEntry->request_method = GET;
	} else if(strstr(tmp_str[map.method], "HEAD") != NULL ) {
		logEntry->request_method = HEAD;
   } else if(strstr(tmp_str[map.method],"POST") != NULL) {
		logEntry->request_method = POST;
	} else if (strstr(tmp_str[map.method],"PROPFIND") != NULL) {
		logEntry->request_method = PROPFIND;
	} else if (strstr(tmp_str[map.method],"PUT") != NULL) {
		logEntry->request_method = PUT;
	} else {
#ifdef DEBUG
		printf("[parse_apache_logEntry]: Invalid method [%s] found in line [%d]\n",tmp, nlineas);
#endif
		return(-1);
	}
#ifdef DEBUG2
		printf("\tMethod  [%d]\n",logEntry->request_method);
#endif
*/
    logEntry->request_method =  check_method(tmp_str[map.method]);
    if (logEntry->request_method == OTHER) {
        printf("[parse_apache_logEntry]: Method [%s] not valid found - Line [%d] \n",tmp_str[map.method],nlineas);
        return(-1);
    }

    /* URI: MANDATORY */
	/* WARNING: MUST CHECK FOR ADDITIONAL FIELDS DUE TO UNCODED SPACES */

    pos = 0;
    if (!tmp_str[map.uri]) {
           printf("[parse_apache_logEntry]: URI not found, line [%d]\n",nlineas);
           return(-1);
    }
    while (tmp_str[map.uri][pos] == '"') { pos++; urilength--;	};				// Eliminate initial "
    if (!strncmp(&tmp_str[map.uri][pos],"http://",7) ) { pos += 6; urilength -= 6;}     // Elimnate http:// to avoid conflicts with rules with :
    if (!strncmp(&tmp_str[map.uri][pos],"https://",8) ) { pos += 7; urilength -= 7;}     // Elimnate http:// to avoid conflicts with rules with :
    memcpy(logEntry->URI, &tmp_str[map.uri][pos], sizeof(char)*(urilength+1));
	logEntry->URI[urilength] = '\0';

    pos = urilength;
    while (logEntry->URI[pos-1] == '\n') { pos--; urilength--; }
    if ((logEntry->URI[pos-1] == '"') && (logEntry->URI[pos-2]  != '\\')) { // Eliminate ending " - In this case, no protocol field is expected
		pos--; urilength--; 
		existprot = false;
	};							
    logEntry->URI[pos] = '\0';
	logEntry->urilen = urilength;
	
	/* Check until next field is protocol */
#ifdef DEBUG2
	printf("URI: [%s] urilength [%d] existprot [%d]\n",logEntry->URI, logEntry->urilen,existprot);
#endif
	
	while ((existprot) && ((map.uri + offset +1) < (n)) && !strstr(tmp_str[map.uri + offset +1],"HTTP") ) {
		
#ifdef DEBUG2
		printf("\t aditional URI: [%s] - index %d\n",tmp_str[map.uri + offset +1],map.uri + offset +1);
#endif
		// Additional URI field
		strcat(logEntry->URI," ");
		urilength = strlen(tmp_str[map.uri + offset +1]);
		if ((tmp_str[map.uri + offset +1][urilength-1] == '"') && (tmp_str[map.uri + offset +1][urilength-2]  != '\\')) { // Eliminate ending " - In this case, no protocol field is expected
			urilength--; 
			existprot = false;
		};			
		strncat(logEntry->URI, tmp_str[map.uri + offset +1],urilength);
		offset++;
		logEntry->urilen += urilength+1;

	}

    /* Protocol: NOT MANDATORY */

//    if (map.protocol > 0)
	if (existprot == true) {
        if (tmp_str[map.protocol + offset])
            if( strstr(tmp_str[map.protocol + offset], "HTTP/1.0") != NULL )
                logEntry->Protocol = _1_0;
            else if( strstr(tmp_str[map.protocol  + offset], "HTTP/1.1") != NULL )
                logEntry->Protocol = _1_1;
            else if( strstr(tmp_str[map.protocol + offset], "HTTP/2") != NULL )
                 logEntry->Protocol = _2;
            else if( strstr(tmp_str[map.protocol + offset], "HTTPS/1.0") != NULL )
                 logEntry->Protocol = S_1_0;
            else if( strstr(tmp_str[map.protocol + offset], "HTTPS/1.1") != NULL )
                 logEntry->Protocol = S_1_1;
            else if( strstr(tmp_str[map.protocol + offset], "HTTPS/2") != NULL )
                 logEntry->Protocol = S_2;
           else {
                logEntry->Protocol = _VOID;
#ifdef DEBUG
                printf("[parse_apache_logEntry]: Protocol field not in list\n");
#endif
            };
	} else {
		logEntry->Protocol = _VOID;
		offset --;
	};

	// Optional fields
	
    if ((map.status_code >= 0) && ((map.status_code + offset) < n)) if (tmp_str[map.status_code + offset]) logEntry->status_code = atoi(tmp_str[map.status_code + offset]);

    if ((map.return_size >= 0) && ((map.return_size + offset) < n)) if (tmp_str[map.return_size + offset]) logEntry->return_size = atoi(tmp_str[map.return_size + offset]);

#ifdef DEBUG
    printf("[parse_apache_logEntry]: status [%d] size [%d]\n", logEntry->status_code, logEntry->return_size);
#endif

    // Optional fields in the log

    if ((map.referer >= 0) && (map.referer + offset < n)) 
        if (tmp_str[map.referer + offset])
            if (!strcmp(tmp_str[map.referer + offset],"-")) {
                logEntry->referer[0] = '\0';
            } else {
#ifdef DEBUG2
		printf("\tReferer string [%s]\n",tmp_str[map.referer + offset]);
#endif
                strcpy(logEntry->referer, tmp_str[map.referer + offset]);
			
				// Additional pieces of referer ?
				
				while ( ((map.referer + offset) < (n-1)) && (tmp_str[map.referer + offset][strlen(tmp_str[map.referer + offset])-1] != '"') ) {
					strcat(logEntry->referer," ");
					strcat(logEntry->referer,tmp_str[map.referer + offset+1]);
					offset ++;
#ifdef DEBUG2
		printf("\tReferer string [%d][%s]\n",map.referer + offset,tmp_str[map.referer + offset]);
#endif
				}
			}
#ifdef DEBUG2
		printf("\tReferer  [%s]\n",logEntry->referer);
#endif
    if ((map.user_agent >= 0)  && (map.user_agent + offset < n)) {
		if (tmp_str[map.user_agent + offset]) {
            if (!strcmp(tmp_str[map.user_agent + offset],"-")) {
                logEntry->user_agent[0] = '\0';
            } else {			
				strcpy(logEntry->user_agent, tmp_str[map.user_agent + offset]);
				more = true;
				while (more && (tmp_str[map.user_agent + offset][strlen(tmp_str[map.user_agent + offset])-1] != '"')) {
					offset++;
					if (map.user_agent + offset < (n)) {
						strcat(logEntry->user_agent," ");
						strcat(logEntry->user_agent,tmp_str[map.user_agent + offset]);
					} else more = false; 
				}

			}
		}
	}

 #ifdef DEBUG
    printf("[parse_apache_logEntry]: URI [%s]\n",logEntry->URI);
#endif
    return(0);

}

/* Parse other formats (not apache alike) */

/* LIST format (optional labels)*/

int parse_list_logEntry(char * logLine, Apache_logEntry * logEntry, log_map map, int logLinelen) {

    char * p;
    int len;

	// Remove trailing \n and spaces 
	
	p = logLine + logLinelen -2;
	while (((*p == '\n') || (*p == ' ')) && (p > logLine)) {
		*p = '\0';
		p--;
		logLinelen --;
	}

	// Label

	if (uri_labels) {
		p = strchr(logLine,']');
		if (p != NULL) {
			len = p - logLine +1;
			strncpy(logEntry->label,logLine, len);
			logEntry->label[len] = '\0';
			p++;
			while ((*p == '\t') || (*p == ' ')) p++;
		} else {
			printf("[parse_list_logEntry]: Error reading label in line [%d] \n",nlineas);
			return(-1);
		};
	} else {
		p = logLine;
		if (*p == '[') {
			printf("[parse_list_logEntry]: Error - Possible label identified in line [%d] \n",nlineas);
			return(-1);
		};	
		logEntry->label[0] = '\0';
	}

	// URI
	// Remove initial http:// an https:// if present
	if (!strncmp(p,"http://",7) ) p += 6;   // Eliminate http[s]:// to avoid conflicts with rules with :
	if (!strncmp(p,"https://",8) ) p += 7;

	len = logLinelen - (p - logLine)-1;
	if (len > URILENGTH) {
        printf("[parse_list_logEntry]: Parse error - line too long [%d] [",nlineas);
		return(-1);
	}
	memcpy(logEntry->URI,p,sizeof(char)*len);
	logEntry->URI[len] = '\0';

	// Cleaning up the end of the uri (it should be a \n\0 sequence)

	if ((logEntry->URI[len-1] == '\n')) {
		logEntry->URI[len-1] = '\0';
		len -= 1;
	}
	logEntry->urilen = len;
    logEntry->request_method = NONE;

#ifdef DEBUG2
    printf("[parse_list_logEntry]: URI [%s] length [%d] medida [%d]\n",logEntry->URI,len, strlen(logEntry->URI));
#endif

 #ifdef DEBUG
    printf("[parse_list_logEntry]: URI [%s]\n",logEntry->URI);
#endif
    return(0);
}

/* ELIST format (optional labels)*/
/* METHOD URI PROT" RES_CODE RES_LEN */

int parse_elist_logEntry(char * logLine, Apache_logEntry * logEntry, log_map map, int logLinelen){

    char * p, *q, *u;
	char tmp[URILENGTH];
    int len, error;

	// Remove trailing \n and spaces 
	
	p = logLine + logLinelen -2;
	while (((*p == '\n') || (*p == ' ')) && (p > logLine)) {
		*p = '\0';
		p--;
		logLinelen --;
	}
	// Label

	if (uri_labels) {
		p = strchr(logLine,']');
		if (p != NULL) {
			len = p - logLine +1;
			strncpy(logEntry->label,logLine, len);
			logEntry->label[len] = '\0';
			p++;
			while ((*p == '\t') || (*p == ' ')) p++;
		} else {
			printf("[parse_elist_logEntry]: Error reading label in line [%d] \n",nlineas);
			return(-1);
		};
	} else {
		p = logLine;
		if (*p == '[') {
			printf("[parse_elist_logEntry]: Error - Possible label identified in line [%d] \n",nlineas);
			return(-1);
		};	
		logEntry->label[0] = '\0';
	}

#ifdef DEBUG2
	printf("Parsing %d chars [%s]\n",logLinelen, logLine);
	printf("\tLabel [%s]\n",logEntry->label);
#endif

	// METHOD - Only valid method are parsed

	q = strchr(p,' ');
    if (!q) {
			printf("[parse_elist_logEntry]: Error spliting fields in line [%d] \n",nlineas);
			return(-1);        
    }
	strncpy(tmp,p,q-p);
	tmp[q-p] = '\0';

/*  Old version - Keep till validation of new version 
	if( strstr(tmp, "GET") != NULL )
		logEntry->request_method = GET;
	else if(strstr(tmp, "HEAD") != NULL )
		logEntry->request_method = HEAD;
	else if(strstr(tmp,"POST") != NULL)
		logEntry->request_method = POST;
	else if (strstr(tmp,"PROPFIND") != NULL)
		logEntry->request_method = PROPFIND;
    else if (strstr(tmp,"PUT") != NULL)
        logEntry->request_method = PUT;
	else {
        printf("[parse_elist_logEntry]: Invalid method [%s] found in line [%d]\n",tmp, nlineas);
		return(-1);
	}
    
    */
    logEntry->request_method =  check_method(tmp);
    if (logEntry->request_method == OTHER) {
        printf("[parse_elist_logEntry]: Invalid method [%s] found in line [%d]\n",tmp, nlineas);
        return(-1);
    }
    
	// Remove initial http[s]:// from uri if present
	q++;
	if (!strncmp(q,"http://",7) ) q += 6;   // Elimnate http:// to avoid conflicts with rules with :
	if (!strncmp(q,"https://",8) ) q += 7;   
	u = q;
#ifdef DEBUG2
	printf("\tPuntero URI->[%s]\n",u);
#endif

	// Resp_len (ignored)

	p = logLine + logLinelen;

#ifdef DEBUG2 
    printf("Buscando hacia atras resp_len desde %s\n",p-1);
#endif
	while ((*p == '\n') || (*p == '\0') ) p--; 
	while ((*p != ' ') && (p > logLine)) p--;

	error = sscanf(p,"%d",&logEntry->return_size);
	if ((error != 1) && ( p[1] != '-')) {
        printf("[parse_elist_logEntry]: Error reading response size [%s] in line [%d] \n",p,nlineas);
		return(-1);
	}

	// Response code

	p--;
#ifdef DEBUG2 
    printf("Buscando hacia atras response code desde %s\n",p);
#endif
	while ((* p != ' ') && (p > logLine)) p--;
	error = sscanf(p,"%d",&logEntry->status_code);
	if ((error != 1) ) {
        printf("[parse_elist_logEntry]: Error reading response code [%s] in line [%d] \n",p,nlineas);
		return(-1);
	}

	// URI (including protocol)
	p--;
	if (*p != '"') {
        printf("[parse_elist_logEntry]: Error in final uri delimiter in line [%d] \n",nlineas);
		return(-1);
	}
	p--;
    
    // Found end of URI HTTPver"
	strncpy(tmp,p-10,10);


	// Protocol (if it exists)

	if( (q=strstr(tmp, "HTTP/1.0")) != NULL )
        logEntry->Protocol = _1_0;
    else if( (q=strstr(tmp, "HTTP/1.1")) != NULL )
        logEntry->Protocol = _1_1;
    else if( (q=strstr(tmp, "HTTP/2")) != NULL )
        logEntry->Protocol = _2;
    else if( (q=strstr(tmp, "HTTPS/1.1")) != NULL )
        logEntry->Protocol = S_1_1;
    else if( (q=strstr(tmp, "HTTPS/2")) != NULL )
        logEntry->Protocol = S_2;     
	else if ( (q=strstr(tmp, "HTTP")) != NULL) 
        logEntry->Protocol = _VOID;     // Unrecognized HTTP version - but keep out of URI
    else {
        logEntry->Protocol = _VOID;
        q=p+1;
    }

    // Now, q points to the end of the URI plus one
    
	// u points to the begining of the URI and p to the end

	len = q-u +1;
	if (len > URILENGTH) {
        printf("[parse_list_logEntry]: Parse error - line too long [%d] [",nlineas);
		return(-1);
	}
	memcpy(logEntry->URI,u,sizeof(char)*len);
	logEntry->URI[len] = '\0';

	// Cleaning up the end of the uri (it should be a \n\0 sequence)

	if ((logEntry->URI[len-1] == '\n')) {
		logEntry->URI[len-1] = '\0';
		len -= 1;
	}
	logEntry->urilen = len-1;

    return(0);

#ifdef DEBUG
    printf("[parse_elist_logEntry]: URI [%s]\n",logEntry->URI);
#endif

}

/* TELIST format (optional labels) - Tab separated */
/* [\t] METHOD  \t URI \t PROT"\t RES_CODE \t RES_LEN */

int parse_telist_logEntry(char * logLine, Apache_logEntry * logEntry, log_map map, int logLinelen){

#define TELIST_TOKENS 5

    char * q;
	char * tmp_str[TELIST_TOKENS+1];
    int n, error, nf, offset;

	// Strict separation in fields, no need to handle special cases as in elist
	//Tokenizer by white-space
    tmp_str[0] = strtok((char *)logLine, "\t");
	nf = (uri_labels) ? TELIST_TOKENS +1 : TELIST_TOKENS;
    for (n=1; n < nf && tmp_str[n-1]!=NULL; n++){
        tmp_str[n] = strtok(NULL, "\t");
#ifdef DEBUG2
        printf("\t[%s]",tmp_str[n]);
#endif
    }
#ifdef DEBUG2
        printf("\n[%d fields] - Urilabels %d\n",n ,uri_labels);
#endif
	
	// Label

	offset = 0;
	if (uri_labels) {
		strcpy(logEntry->label,tmp_str[0]);
		offset = 1;
	} else {
		logEntry->label[0] = '\0';
		offset = 0;
	}
#ifdef DEBUG2
	printf("\tLabel [%s]\n",logEntry->label);
#endif
	// METHOD - Only valid methods are parsed

/* Old version 
	if( strstr(tmp_str[offset], "GET") != NULL )
		logEntry->request_method = GET;
	else if(strstr(tmp_str[offset], "HEAD") != NULL )
		logEntry->request_method = HEAD;
	else if(strstr(tmp_str[offset],"POST") != NULL)
		logEntry->request_method = POST;
	else if (strstr(tmp_str[offset],"PROPFIND") != NULL)
		logEntry->request_method = PROPFIND;
    else if (strstr(tmp_str[offset],"PUT") != NULL)
        logEntry->request_method = PUT;
	else {
        printf("[parse_elist_logEntry]: Invalid method [%s] found in line [%d]\n",tmp_str[offset], nlineas);
		return(-1);
	}
	*/
     logEntry->request_method =  check_method(tmp_str[offset]);
    if (logEntry->request_method == OTHER) {
        printf("[parse_elist_logEntry]: Invalid method [%s] found in line [%d]\n",tmp_str[offset], nlineas);
        return(-1);
    }
#ifdef DEBUG2
	printf("\tMethod [%s]\n",tmp_str[offset]);
#endif

	// URI
	// Remove initial http[s]:// from uri if present
	
	offset ++;
	q = tmp_str[offset];
	if (!strncmp(q,"http://",7) ) q += 7;   // Elimnate http:// to avoid conflicts with rules with :
	if (!strncmp(q,"https://",8) ) q += 8;   
	
	strcpy(logEntry->URI,q);
	logEntry->urilen = strlen(logEntry->URI);
#ifdef DEBUG2
	printf("\tURI [%s] q [%s]\n",logEntry->URI, q);
#endif

	// Protocol (if it exists)

	offset++;
	if( strstr(tmp_str[offset], "HTTP/1.0") != NULL )
        logEntry->Protocol = _1_0;
    else if( strstr(tmp_str[offset], "HTTP/1.1") != NULL )
        logEntry->Protocol = _1_1;
    else if( strstr(tmp_str[offset], "HTTP/2") != NULL )
        logEntry->Protocol = _2;
    else if( (strstr(tmp_str, "HTTPS/1.1")) != NULL )
        logEntry->Protocol = S_1_1;
    else if( (strstr(tmp_str, "HTTPS/2")) != NULL )
        logEntry->Protocol = S_2;     
	else {
        logEntry->Protocol = _VOID;
    }

	// Response code  

	offset ++;
	if (strcmp(tmp_str[offset],"-")) {
		error = sscanf(tmp_str[offset],"%d",&logEntry->status_code);
		if ((error != 1)) {
			printf("[parse_elist_logEntry]: Error reading response size [%s] in line [%d] \n",tmp_str[offset],nlineas);
			return(-1);
		}
	} else logEntry->status_code = 0;
#ifdef DEBUG2
	printf("\tCR [%d]\n",logEntry->status_code);
#endif
	// Response code

	offset ++;
	if (strcmp(tmp_str[offset],"-")) {
		error = sscanf(tmp_str[offset],"%d",&logEntry->return_size);
		if ((error != 1)) {
			printf("[parse_elist_logEntry]: Error reading response size [%s] in line [%d] \n",tmp_str[offset],nlineas);
			return(-1);
		}
	} else logEntry->return_size = 0;
#ifdef DEBUG2
	printf("\tRS [%d]\n",logEntry->return_size);
#endif
	
	// Cleaning up the end of the uri (it should be a \n\0 sequence)

	if ((logEntry->URI[logEntry->urilen-1] == '\n')) {
		logEntry->URI[logEntry->urilen-1] = '\0';
		logEntry->urilen -= 1;
	}

    return(0);
#ifdef DEBUG
    printf("[parse_telist_logEntry]: URI [%s]\n",logEntry->URI);
#endif
#undef DEBUG2
}

/* URI format (optional labels)*/

int parse_uri_logEntry(char * logLine, Apache_logEntry * logEntry, log_map map, int logLinelen){

    char * p, *q;
    int len, error;

	// Label

	if (uri_labels) {
		p = strchr(logLine,']');
		if (p != NULL) {
			len = p - logLine +1;
			strncpy(logEntry->label,logLine, len);
			logEntry->label[len] = '\0';
			p++;
			while ((*p == '\t') || (*p == ' ')) p++;
		} else {
			printf("[parse_uri_logEntry]: Error reading label in line [%d] \n",nlineas);
			return(-1);
		};
	} else {
		p = logLine;
		if (*p == '[') {
			printf("[parse_uri_logEntry]: Error - Possible label identified in line [%d] \n",nlineas);
			return(-1);
		};	
		logEntry->label[0] = '\0';
	}

	// Length of the uri

	q = strchr(p,' ');
	if (q == NULL) {
		printf("[parse_uri_logEntry]: Error reading size in line [%d] \n",p,nlineas);
		return(-1);
	};
	error = sscanf(p,"%d",&len);
	if (error != 1) {
        printf("[parse_uri_logEntry]: Error reading uri size [%s] in line [%d] \n",p,nlineas);
		return(-1);
	}
	q++;

	// URI
	// Remove initial http:// if present
	if (!strncmp(q,"http://",7) ) { // Elimnate http:// to avoid conflicts with rules with :
		q += 6;
		len -= 6;
	}; 
	if (!strncmp(q,"https://",8) ) { q += 7; len -= 7; }


	if (len > URILENGTH) {
        printf("[parse_uri_logEntry]: Parse error - line too long [%d]\n",nlineas);
		return(-1);
	}
	memcpy(logEntry->URI,q,sizeof(char)*len);
	logEntry->URI[len] = '\0';

	// Cleaning up the end of the uri (it should be a \n\0 sequence)

	while ( (logEntry->URI[len-1] == '\n')) {
		logEntry->URI[len-1] == '\0';
		len -= 1;
	}
	logEntry->urilen = len;
    logEntry->request_method = NONE;
    return(0);

 #ifdef DEBUG
    printf("[parse_uri_logEntry]: URI [%s]\n",logEntry->URI);
#endif

}

/* Empty log record */

void init_Apache_logEntry(Apache_logEntry * logEntry){

    logEntry->ip_address[0] = '\0';
    logEntry->user_identifier[0] = '\0';
    logEntry->user_id[0] = '\0';
    logEntry->request_method = NONE;
    logEntry->URI[0] = '\0';
    logEntry->Protocol = 0;
    logEntry->status_code = 0;
    logEntry->return_size = 0;

    /* --- Additional fields for Combined Log Format ---*/
    logEntry->referer[0] = '\0';
    logEntry->user_agent[0] = '\0';
	logEntry->label[0] = '\0';

}

int compare( const void* a, const void* b)
{
    unsigned int int_a = * ( (unsigned int*) a );
    unsigned int int_b = * ( (unsigned int*) b );

     if ( int_a == int_b ) return 0;
     else if ( int_a < int_b ) return -1;
     else return 1;
}

