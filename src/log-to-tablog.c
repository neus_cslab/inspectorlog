/*
** INSPECTORLOG
** Todos los derechos reservados
** All rights reserved
**
** Copyright (C) 2017-2022, Jesús E. Díaz Verdejo
** Version 3.5.2 JEDV - - 21/09/2022
** Version 3.5.1 JEV - 07/08/2022
** Version 3.5 JEDV - 24/02/2022
** Versión 3.4 JEDV - 25/11/2021
** Versión 3.0 JEDV - 19/12/2017
** 
** log-to-tablog
** Reads any of the supported input formats and outputs a tab separated fields format
** 
** v3.5.2: 03/02/2023: BUG in TELIST output (ending 'i' in every URI) solved
**   v3.5.2: Added strict response code filtering (CR<300)
**  v3.5.2: Added (unneded) attack_file
**  v3.5.1: Added summary of analyzed lines (n. uris, n. filtered, n. malformed)
**  v3.5.1: Bug writing TELIST when no protocol is set
** Alfa version 27/03/2022
*/

//some extra functions that are defined in the X/Open and POSIX standards.
#define _XOPEN_SOURCE 700

#define _GNU_SOURCE

//C INCLUDES
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <dirent.h>
#include <limits.h>
#include <ftw.h>
#include <getopt.h>

//INSPECTORLOG INCLUDES
#include <inspector-common.h>
#include <inspector.h>

// GLOBAL VARIABLES 

#undef DEBUG
#undef DEBUG2

// Options (default)

bool outputf = false;
int log_type=LOG_APACHE;	// APACHE format by default 
bool uri_labels = false;
bool ouri_labels = false;
bool resp_code = false;
bool strict_resp_code = false;
bool separa_query = false;
int output_log_type = -1;
int filtrados = 0;
int erroneos = 0;
bool output_erroneous = false;

// Unneeded variables (compatibility with inspector-common.h) 

bool nocase;
bool ealert;
bool warns;
int nlineas;
time_t rawtime;
struct tm *timeinfo;
unsigned char attack_file[PATH_MAX+1];
FILE *faout;

// Files/Input/Output

unsigned char log_path[PATH_MAX+1];
unsigned char output_file[PATH_MAX+1];

bool clArgs(int argc, char **argv){

    bool isOK = true;

    int c;

    if(argc < 3) {
		printf("FORMAT: log-to-tablog -l logFile [-t <list|elist|apache|wellness|uri>] -o salida [-s <list|elist|apache|wellness|uri>][-c (response code filtering)] [-b (input labeled uris)] [-u (unlabeled output)] [-q (split path/query)] [-e (list erroneous)\n");

		exit(EXIT_SUCCESS);	
	}
    while (1){

        static struct option long_options[] =
             {
               /* These options don't set a flag.
                  We distinguish them by their indices. */
               {"log_file",  required_argument, 0, 'l'},
               {"logtype", required_argument, 0, 't'},
               {"output", required_argument, 0, 'o'},
			   {"resp_code", no_argument, 0, 'c'},
               {"strict_resp_code", no_argument, 'x'},
			   {"labels", no_argument, 0, 'b'},
 			   {"query", no_argument, 0, 'q'},
 			   {"olabels", no_argument, 0, 'u'},
               {"output_logtype", required_argument, 0, 's'},
               {"erroneous",no_argument, 0, 'e'},
			   {0, 0, 0, 0}
             };
           /* getopt_long stores the option index here. */
           int option_index = 0;

        c = getopt_long(argc, argv, "l:t:o:xcbqus:e", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

#ifdef DEBUG
            printf("Option detected %c, %s\n",c,optarg);
#endif        
        switch (c){

            case 'l':
                //printf ("option -l with value '%s'\n", optarg);
                strncpy((char*)&log_path, optarg, PATH_MAX);
                break;
            case 't':
                if (!strcmp(optarg,"list")) {
                    log_type = LOG_LIST;
                } else if (!strcmp(optarg,"apache")) {
                    log_type = LOG_APACHE;
                } else if (!strcmp(optarg,"wellness")) {
                    log_type = LOG_WELLNESS;
                } else if (!strcmp(optarg,"uri")) {
                    log_type = LOG_URI;
				} else if (!strcmp(optarg,"elist")) {
					log_type = LOG_ELIST;
                } else {
                    printf("log-to-tablog: log format [%s] not recognized\n",optarg);
                    exit(-1);
                }
                break;
            case 'o':
                strncpy((char *)&output_file, optarg, PATH_MAX);
                outputf = true;
                break;
			case 'c':
				strict_resp_code = false;
				resp_code = true;
				break;
			case 'x':
				strict_resp_code = true;
                resp_code = false;
				break;
            case 'q':
				separa_query = true;
				break;
			case 'b':
				uri_labels = true;
				break;
			case 'u':
				ouri_labels = true;
				break;
            case 'e':
				output_erroneous = true;
				break;
			case 's':
                if (!strcmp(optarg,"list")) {
                    output_log_type = LOG_LIST;
                } else if (!strcmp(optarg,"apache")) {
                    output_log_type = LOG_APACHE;
                } else if (!strcmp(optarg,"wellness")) {
                    output_log_type = LOG_WELLNESS;
                } else if (!strcmp(optarg,"uri")) {
                    output_log_type = LOG_URI;
				} else if (!strcmp(optarg,"elist")) {
					output_log_type = LOG_ELIST;
                } else {
                    printf("log-to-tablog: output log format [%s] not recognized\n",optarg);
                    exit(-1);
                }
                break;
            default:
                printf("FORMAT: log-to-tablog -l logFile [-t <list|elist|apache|wellness|uri>] -o salida [-s <list|elist|apache|wellness|uri>][-c (response code filtering)] [-b (input labeled uris)] [-u (unlabeled output)] [-q (split path/query)] [-e (list erroneous)\n");
				exit(EXIT_SUCCESS);
		}
    }
#ifdef DEBUG
    printf(">> Command line arguments processed ...\n");
#endif

    /* Print any remaining command line arguments (not options). */
    if (optind < argc){
           printf ("[parse_clArgs] Argument(s) erroneous: ");
           while (optind < argc)
             printf ("%s ", argv[optind++]);
           putchar ('\n');
    }

    return isOK;
}

/************************************************/
/* Memory management                            */
/************************************************/

unsigned char * uchar_malloc(int num_bytes){

    unsigned char * ptr;

    ptr = (unsigned char*) malloc(num_bytes);
    if (ptr == NULL){
        printf("[Memory] Error in 'uchar_malloc': Memory reservation failure \n");
        exit(EXIT_FAILURE);
    }
    return ptr;
}

/************************************************/
/* MAIN PROGRAM                          */
/************************************************/

int main(int argc, char **argv){

	char query[URILENGTH];
	char time[WORDLENGTH];
	char method[WORDLENGTH];
	char prot[WORDLENGTH];
	char *q;
	char *logLine;
	int lineLength;
	log_map map;
	int tmp, parsed;
	ssize_t read;
	int npackets;
	
    //Default arguments

#ifdef DEBUG
    printf("> Processing start\n");
#endif
    
    //Parse command line arguments
    if( ! clArgs(argc, argv))
        exit(EXIT_FAILURE);

#ifdef DEBUG
    printf("> Arguments read ...\n");
#endif

	// Check for invalid input/output combinations 
	
	if (log_type == LOG_APACHE) {
		if (output_log_type == -1) output_log_type = LOG_APACHE;
	} else if (log_type == LOG_WELLNESS) {
		if (output_log_type == -1) output_log_type = LOG_WELLNESS;
	} else if (log_type == LOG_ELIST) {
		if (output_log_type == -1) output_log_type = LOG_ELIST;
		else if ((output_log_type == LOG_WELLNESS) || (output_log_type == LOG_APACHE)) {
			printf("ERROR: Invalid input/output types combination\n");
			exit(-1);
		};
	} else if (log_type == LOG_LIST) {
		if (output_log_type == -1) output_log_type = LOG_LIST;
		else if ((output_log_type == LOG_WELLNESS) || (output_log_type == LOG_APACHE) || (output_log_type == LOG_ELIST)) {
			printf("ERROR: Invalid input/output types combination\n");
			exit(-1);
		};
	} else if (log_type == LOG_URI) {
		if (output_log_type == -1) output_log_type = LOG_WELLNESS;
		else if ((output_log_type == LOG_WELLNESS) || (output_log_type == LOG_APACHE) || (output_log_type == LOG_ELIST)) {
			printf("ERROR: Invalid input/output types combination\n");
			exit(-1);
		};
	}
	if (!(uri_labels)) {
		ouri_labels = true;
	} 
		
    // Input / output files

    FILE * logFile = fopen(log_path, "r");
	FILE *fout;
    Apache_logEntry logEntry;

    if(logFile != NULL){

        logLine = (char*) malloc (sizeof(char)*MAXLOG_LINE);
		lineLength = MAXLOG_LINE*sizeof(char);

        // Initialize log map

        init_log_map(&map);

        if (outputf) {
#ifdef DEBUG
            printf("Opening clean output file <%s>\n",output_file);
#endif
            fout = fopen(output_file,"w");
            if (!fout) {
                printf("[scan_logFile]: ERROR - Opening output file [%s]\n",output_file);
                exit(-1);
            }
#ifdef DEBUG
            printf(" ... Done \n");
#endif
        } else fout = stdout;
		
		// Output number of URIs (or room for it) for LOG_URI format
		
        if (log_type == LOG_URI) {
			read = getline(&logLine, &lineLength, logFile);
			tmp = sscanf(logLine,"%d\n",&npackets);
			if (tmp != 1) {
                printf("[scan_logFile]: ERROR reading the number of lines [%s]\n",output_file);
                exit(-1);
			}
		} else npackets = 0;
			
			// Write in the header the number of lines (maximum is the read value) - 1M lines top (output formatting)
        if (output_log_type == LOG_URI) {			
			if (outputf) fprintf(fout,"%8d\n",npackets);
    	};
		
        // Read and process each line

        for(int i=1; (read = getline(&logLine, &lineLength, logFile)) != -1; i++) {

 			if (read > MAXLOG_LINE) {
					printf("[scan_LogFile]: WARNING - Line [%d] too long (%d chars) - DISMISSED\n", nlineas, read);
                    erroneos ++;
					continue;
			}
			read++;		// To count for '\0'
            nlineas++;

            // Line parsing

#ifdef DEBUG
            printf("Parsing line [%d] [%s]\n",i,logLine);
#endif
            init_Apache_logEntry(&logEntry);

            if ((log_type == LOG_APACHE) || (log_type == LOG_WELLNESS)) parsed = parse_apache_logEntry(logLine, &logEntry, map, read);
			else if (log_type == LOG_URI) parsed = parse_uri_logEntry(logLine, &logEntry, map, read) ;
			else if (log_type == LOG_LIST) parsed = parse_list_logEntry(logLine, &logEntry, map, read);
			else if (log_type == LOG_ELIST) parsed = parse_elist_logEntry(logLine, &logEntry, map, read);
			if (parsed == -1) {
//				npackets ++;
                if (output_erroneous) printf("[PARSE] - Line [%d] - %s\n",nlineas, logLine);
                erroneos ++;
				continue;
			};

			if ((resp_code) && (logEntry.status_code >= RESP_CODE_INVALID) ) {
                filtrados++;
                continue;
			} else if ((strict_resp_code) && (logEntry.status_code >= RESP_CODE_STRICT) ) {
                filtrados++;
                continue;
			} else npackets++;
			
			// Print output 

			if (separa_query) {
				q = strchr(logEntry.URI,'?'); 
				if (q) {
					strcpy(query,q+1);
					logEntry.URI[q-logEntry.URI]='\0';
				} else {
					query[0] = '\0';
				}
			};
		
			if ((log_type == LOG_APACHE) || (log_type == LOG_WELLNESS) || (log_type == LOG_ELIST)) {
			/* OLD Version 
				if (logEntry.request_method == GET) strcpy(method,"GET");
				else if (logEntry.request_method == POST) strcpy(method,"POST");
				else if (logEntry.request_method == HEAD) strcpy(method,"HEAD");
				else if (logEntry.request_method == PROPFIND) strcpy(method,"PROPFIND");
				else if (logEntry.request_method == PUT) strcpy(method,"PUT");
				else if (logEntry.request_method == NONE) strcpy(method,"NONE");
				else {
						printf("[log-to-tablog: ERROR unknown method in line [%d]\n",nlineas);
						exit(-1);
				} */
				if (logEntry.request_method != NONE) {
					method_str(logEntry.request_method, method);
				} else {
					printf("[log-to-tablog: ERROR unknown method in line [%d]\n",nlineas);
					exit(-1);
				};
				
			}
			
			if (logEntry.Protocol == _1_0) strcpy(prot,"HTTP/1.0\"");
			else if (logEntry.Protocol == _1_1) strcpy(prot,"HTTP/1.1\"");
			else if (logEntry.Protocol == _2) strcpy(prot,"HTTP/2\"");
			else if (logEntry.Protocol == S_1_0) strcpy(prot,"HTTPS/1.0\"");
			else if (logEntry.Protocol == S_1_1) strcpy(prot,"HTTPS/1.1\"");
			else if (logEntry.Protocol == S_2) strcpy(prot,"HTTPS/2\"");
			else strcpy(prot,"\"");
			
			if (!ouri_labels) fprintf(fout,"%s\t",logEntry.label);
			if (output_log_type == LOG_APACHE) {
				strftime(time,WORDLENGTH,"[%d/%b/%Y%T %z]",&logEntry.time);
				fprintf(fout,"%s\t%s\t%s\t%s\t%s\t%s\t",logEntry.ip_address,logEntry.user_identifier,logEntry.user_id,time,method,logEntry.URI);
				if (separa_query) {
					if (query[0] != '\0') fprintf(fout,"%s",query);
					else fprintf(fout,"\t");
				};
                fprintf(fout,"\t%s\t%d\t%d\t%s\t%s\n",prot,logEntry.status_code,logEntry.return_size,logEntry.referer,logEntry.user_agent);
				
			} else if (output_log_type == LOG_WELLNESS) {				
				fprintf(fout,"TODO\n");
			} else if (output_log_type == LOG_URI) {
				fprintf(fout, "%d\t%s",strlen(logEntry.URI),logEntry.URI);
				if (separa_query) {
					if (query[0] != '\0') fprintf(fout,"\t%s\n",query);
					else fprintf(fout,"\t\n");
				} else fprintf(fout,"\n");									
			} else if (output_log_type == LOG_LIST) {
				fprintf(fout, "%s",logEntry.URI);
				if (separa_query) {
					if (query[0] != '\0') fprintf(fout,"\t%s\n",query);
					else fprintf(fout,"\t\n");
				} else fprintf(fout,"\n");									
			} else if (output_log_type == LOG_ELIST) {
				fprintf(fout, "%s\t%s",method,logEntry.URI);
				if (separa_query) {
					if (query[0] != '\0') fprintf(fout,"\t%s\t",query);
					else fprintf(fout,"\t\t");
				};	
              
                fprintf(fout,"\t%s\t%d\t%d\n",prot,logEntry.status_code,logEntry.return_size);
			};
			
		};
		
        if (outputf) {
			if (output_log_type == LOG_URI) {	// If LOG_URI type, adjust the number of registers (1st line)
				rewind(fout);
				fprintf(fout,"%8d\n",npackets);
			};
        };
	
		// Cleaning 
		
        printf("[log-to-tablog]: [%s] - Processed [%d] lines, [%d] stored, [%d] filtered out and [%d] erroneous\n",log_path,nlineas,npackets,filtrados,erroneos);
	    free(logLine);
        fclose(logFile);
		if (outputf) fclose(fout);

    } else {
        printf("[log-to-tablog]: ERROR - Log path is incorrect = %s\n", log_path);
    }

    return 0;
}
